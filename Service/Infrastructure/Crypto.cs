using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace Service.Infrastructure
{
	public sealed class Crypto
	{
		
		private const int TokenSizeInBytes = 16;
		private const int Pbkdf2Count = 1000;
		private const int Pbkdf2SubkeyLength = 256 / 8;
		private const int SaltSize = 128 / 8;
		
		internal static string GenerateToken()
		{
			var tokenBytes = new byte[TokenSizeInBytes - 1+ 1];
			using (var prng = new RNGCryptoServiceProvider())
			{
				prng.GetBytes(tokenBytes);
				return Convert.ToBase64String(tokenBytes);
			}
			
		}
		
		public static string GenerateSalt(int byteLength = 128 / 8)
		{
			var buff = new byte[byteLength - 1+ 1];
			using (var prng = new RNGCryptoServiceProvider())
			{
				prng.GetBytes(buff);
			}
			
			return Convert.ToBase64String(buff);
		}
		
		public static string Hash(string input, string algorithm = "sha256")
		{
			if (string.IsNullOrEmpty(input))
			{
                throw (new ArgumentNullException("input"));
			}
			return Hash(Encoding.UTF8.GetBytes(input), algorithm);
		}
		
		public static string Hash(byte[] input, string algorithm = "sha256")
		{
			if (input == null)
			{
                throw (new ArgumentNullException("input"));
			}
			
			using (var alg = HashAlgorithm.Create(algorithm))
			{
			    if (alg == null)
			    {
			        throw (new InvalidOperationException(
			            string.Format("Not supported hash algorhitm {0}", algorithm)));
			    }
			    byte[] hashData = alg.ComputeHash(input);
			    return BinaryToHex(hashData);
			}
			
		}
		
		public static string Sha1(string input)
		{
			return Hash(input, "sha1");
		}
		
		public static string Sha256(string input)
		{
			return Hash(input, "sha256");
		}
		
		//=======================
		//HASHED PASSWORD FORMATS
		//=======================
		//Version 0:
		//PBKDF2 with HMAC-SHA1, 128-bit salt, 256-bit subkey, 1000 iterations.
		//See also: SDL crypto guidelines v5.1, Part III)
		//Format: { 0x00, salt, subkey }
		
		public static string HashPassword(string password)
		{
			if (string.IsNullOrEmpty(password))
			{
                throw (new ArgumentNullException("password"));
			}
			
			byte[] salt;
			byte[] subKey;
			using (var deriveBytes = new Rfc2898DeriveBytes(password, Convert.ToInt32(SaltSize), Convert.ToInt32(Pbkdf2Count)))
			{
				salt = deriveBytes.Salt;
				subKey = deriveBytes.GetBytes(Convert.ToInt32(Pbkdf2SubkeyLength));
			}
			
			
			var outputBytes = new byte[1 + SaltSize + (Pbkdf2SubkeyLength - 1)+ 1];
			Buffer.BlockCopy(salt, 0, outputBytes, 1, Convert.ToInt32(SaltSize));
			Buffer.BlockCopy(subKey, 0, outputBytes, Convert.ToInt32(1 + SaltSize), Convert.ToInt32(Pbkdf2SubkeyLength));
			return Convert.ToBase64String(outputBytes);
		}
		
		//HashedPassword must be of the format of HashWithPassword (Salt + Hash(Salt+Input)
		public static bool VerifyHashedPassword(string hashedPassword, string password)
		{
            if (string.IsNullOrEmpty(hashedPassword))
			{
                throw (new ArgumentNullException("hashedPassword"));
			}
			if (string.IsNullOrEmpty(password))
			{
                throw (new ArgumentNullException("password"));
			}
			
			byte[] hashedPasswordBytes = Convert.FromBase64String(hashedPassword);
			
			if (hashedPasswordBytes.Length != (1 + SaltSize + Pbkdf2SubkeyLength) || hashedPasswordBytes[0] != 0x0)
			{
				//Wrong length or version header.
				return false;
			}
			
			var salt = new byte[SaltSize - 1+ 1];
			Buffer.BlockCopy(hashedPasswordBytes, 1, salt, 0, Convert.ToInt32(SaltSize));
			var storedSubkey = new byte[Pbkdf2SubkeyLength - 1+ 1];
			Buffer.BlockCopy(hashedPasswordBytes, Convert.ToInt32(1 + SaltSize), storedSubkey, 0, Convert.ToInt32(Pbkdf2SubkeyLength));
			
			byte[] generatedSubkey;
			using (var deriveBytes = new Rfc2898DeriveBytes(password, salt, Convert.ToInt32(Pbkdf2Count)))
			{
				generatedSubkey = deriveBytes.GetBytes(Convert.ToInt32(Pbkdf2SubkeyLength));
			}
			
			return ByteArraysEqual(storedSubkey, generatedSubkey);
		}
		
		internal static string BinaryToHex(byte[] data)
		{
			var hex = new char[data.Length * 2 - 1+ 1];
			
			for (var iter = 0; iter <= data.Length - 1; iter++)
			{
				var hexChar = Convert.ToByte(data[iter] >> 4);
				hex[iter * 2] = (char)(Convert.ToInt32(hexChar > 9 ? hexChar + 0x37 : hexChar + 0x30));
				hexChar = Convert.ToByte(data[iter] & 0xF);
                hex[iter * 2 + 1] = (char)(Convert.ToInt32(hexChar > 9 ? hexChar + 0x37 : hexChar + 0x30));
			}
			return new string(hex);
		}
		
		//Compares two byte arrays for equality. The method is specifically written so that the loop is not optimized.
		[MethodImpl(MethodImplOptions.NoOptimization)]private static bool ByteArraysEqual(byte[] a, byte[] b)
		{
			if (ReferenceEquals(a, b))
			{
				return true;
			}
			
			if (a == null || b == null || a.Length != b.Length)
			{
				return false;
			}
			
			var areSame = true;
			for (var i = 0; i <= a.Length - 1; i++)
			{
				areSame = areSame && (a[i] == b[i]);
			}
			return areSame;
		}
		
	}
}
