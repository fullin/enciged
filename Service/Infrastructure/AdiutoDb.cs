﻿using System.Data;
using System.Data.Common;
using NPoco;

namespace Service.Infrastructure
{
    public class AdiutoDb : Database
    {
        public AdiutoDb() : base("Adiuto_Conn")
        {
        }

        public AdiutoDb(IDbConnection connection)
            : base(connection)
        {
        }

        public AdiutoDb(string connectionString, string providerName)
            : base(connectionString, providerName)
        {
        }

        public AdiutoDb(string connectionString, DbProviderFactory provider)
            : base(connectionString, provider)
        {
        }

        public AdiutoDb(string connectionStringName)
            : base(connectionStringName)
        {
        }
    }
}