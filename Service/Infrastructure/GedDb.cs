﻿using System.Data;
using System.Data.Common;
using NPoco;

namespace Service.Infrastructure
{
    public class GedDb : Database
    {
        public GedDb() : base("Ged_Conn")
        {
        }

        public GedDb(IDbConnection connection)
            : base(connection)
        {
        }

        public GedDb(string connectionString, string providerName)
            : base(connectionString, providerName)
        {
        }

        public GedDb(string connectionString, DbProviderFactory provider)
            : base(connectionString, provider)
        {
        }

        public GedDb(string connectionStringName)
            : base(connectionStringName)
        {
        }
    }
}