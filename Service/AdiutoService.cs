﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NPoco;
using Oracle.ManagedDataAccess.Client;
using Service.Models;

namespace Service
{
    public interface IAdiutoService
    {
        UtentiAdiuto GetUserData(string username, string password);
        IEnumerable<UtentiAdiuto> GetUserList();
    }

    public class AdiutoService : IAdiutoService
    {
        private IDatabase _db;
        private IGedService _gedService;
        public AdiutoService(IDatabase db, IGedService gedService)
        {
            _db = db;
            _gedService = gedService;
        }

        public UtentiAdiuto GetUserData(string username, string password)
        {
            //Adiuto su Microsoft SQL Server
            //var user = _db.FirstOrDefault<UtentiAdiuto>(@"SELECT TUSE.*, CASE WHEN FSTS = 'IN_FORZA' THEN 0 ELSE 1 END AS SOSPESO
            //           FROM TUSE WHERE FUNA = @0 AND FPAS = @1 AND FSTS = 'IN_FORZA' AND FUNA <> 'Admin'", username, password);
            //if (user != null)
            //    user.UtenteAvanzato = _gedService.IsUtenteAvanzato(user.Id);
            //return user;

            //Adiuto su Oracle
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Adiuto_Conn"].ConnectionString))
            {
                conn.Open();
                var sql = @"SELECT FPAS, FIDS, FUNA, FIUS, FADM, FEMA, FLAN, FSTS, SSO, CASE WHEN FSTS = 'IN_FORZA' THEN 0 ELSE 1 END AS SOSPESO
                             FROM TUSE WHERE (FUNA = :A) AND (FPAS = :B) AND (FSTS = 'IN_FORZA')"; // AND (NOT UPPER(FUNA) = 'ADMIN')

                using (var comm = new OracleCommand(sql, conn))
                {
                    comm.Parameters.Add(new OracleParameter
                    {
                        ParameterName = "A",
                        Value = username
                    });
                    comm.Parameters.Add(new OracleParameter
                    {
                        ParameterName = "B",
                        Value = password
                    });

                    using (var rdr = comm.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            var ret = new UtentiAdiuto
                            {
                                Id = Convert.ToInt32(rdr[3]),
                                Password = (string)rdr[0],
                                Username = (string)rdr[2],
                                AmministratoreAdiuto = Convert.ToInt32(rdr[4]),
                                Email = (string)rdr[5],
                                Sospeso = Convert.ToBoolean(rdr[9])
                            };
                            conn.Close();
                            ret.UtenteAvanzato = _gedService.IsUtenteAvanzato(ret.Id);
                            return ret;
                        }
                        conn.Close();
                        return null;
                    }
                }
            }
        }

        public IEnumerable<UtentiAdiuto> GetUserList()
        {
            //Adiuto su Microsoft SQL Server
            //var ret = _db.Query<UtentiAdiuto>(@"SELECT '' AS FPAS, FIDS, FUNA, FIUS, FADM, FEMA, FLAN, FSTS, SSO, CASE WHEN FSTS = 'IN_FORZA' THEN 0 ELSE 1 END AS SOSPESO
            //                                    FROM TUSE WHERE FSTS = 'IN_FORZA' AND FUNA <> 'Admin'");
            //return ret.OrderBy(t => t.Username);

            //Adiuto su Oracle
            using (var conn = new OracleConnection(ConfigurationManager.ConnectionStrings["Adiuto_Conn"].ConnectionString))
            {
                conn.Open();
                string sql = @"SELECT '' AS FPAS, FIDS, FUNA, FIUS, FADM, FEMA, FLAN, FSTS, SSO, CASE WHEN FSTS = 'IN_FORZA' THEN 0 ELSE 1 END AS SOSPESO
                               FROM TUSE WHERE FSTS = 'IN_FORZA' AND (NOT UPPER(FUNA) = 'ADMIN')";
                using (var comm = new OracleCommand(sql, conn))
                {
                    using (var rdr = comm.ExecuteReader())
                    {
                        var ret = new List<UtentiAdiuto>();
                        while (rdr.Read())
                        {
                            var item = new UtentiAdiuto
                            {
                                Id = Convert.ToInt32(rdr[3]),
                                //Password = (string)rdr[0],
                                Username = (string)rdr[2],
                                AmministratoreAdiuto = Convert.ToInt32(rdr[4]),
                                Email = (string)rdr[5],
                                Sospeso = Convert.ToBoolean(rdr[9])
                            };
                            item.UtenteAvanzato = _gedService.IsUtenteAvanzato(item.Id);
                            ret.Add(item);
                        }
                        conn.Close();
                        return ret.OrderBy(t => t.Username);
                    }
                }
            }
        }
    }
}
