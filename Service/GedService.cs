﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using NPoco;
using Service.Models;
using System.Linq;

namespace Service
{
    public interface IGedService
    {
        //Documenti
        IEnumerable<Documenti> GetDocumenti(int idEvento, bool gestore, bool vedeRiservati, int idUtente);
        int AddDocumenti(Documenti model);
        void UpdateDocumenti(Documenti model);
        void DeleteDocumenti(int id);
        Documenti GetDocumentiById(int id);
        List<Documenti> GetDocumentiRicercaAvanzata(int idOrganizzazione, bool vedeRiservati);
        List<Documenti> GetDocumentiInFascicolo(int idFascicolo, bool soloAperti);

        //Eventi
        IEnumerable<Eventi> GetEventi(int idOrganizzazione);
        IEnumerable<Eventi> GetEventi(int idOrganizzazione, DateTime da, DateTime a, bool gestore, DateTime utenteValidoDal, DateTime utenteValidoAl);
        int AddEventi(Eventi model);
        void UpdateEventi(Eventi model);
        void DeleteEventi(int id);
        Eventi GetEventiById(int id);

        //Organizzazioni
        IEnumerable<Organizzazioni> GetOrganizzazioni();
        IEnumerable<Organizzazioni> GetOrganizzazioni(int idUtente);
        int AddOrganizzazioni(Organizzazioni model);
        void UpdateOrganizzazioni(Organizzazioni model);
        void DeleteOrganizzazioni(int id);
        Organizzazioni GetOrganizzazioniById(int id);

        //Immagini - Icone
        int InsertImmagine(Immagini immagine);
        void DeleteImmagine(int id);
        void DeleteImmagine(string idImmagine);
        IEnumerable<Immagini> GetIcone();
        string GetIdIcona(string fileName);

        //AssEventiDocumenti
        int GetAssEventiDocumenti(int idDocumento, int idEvento);
        int AddAssEventiDocumenti(AssEventiDocumenti model);
        void DeleteAssEventiDocumenti(int id);
        void DeleteAssEventiDocumenti(int idDocumento, int idEvento);
        void BulkAddAssEventiDocumenti(List<AssEventiDocumenti> associazioni);

        //AssOrganizzazioniUtenti
        IEnumerable<AssOrganizzazioniUtenti> GetAssOrganizzazioniUtenti(int idOrganizzazione);
        int AddAssOrganizzazioniUtenti(AssOrganizzazioniUtenti model);
        void DeleteAllAssOrganizzazioniUtenti(int idOrganizzazione);
        void BulkAddAssOrganizzazioniUtenti(List<AssOrganizzazioniUtenti> associazioni); //Non va
        bool IsUtenteAvanzato(int userId);

        //Fascicoli
        IEnumerable<Fascicoli> GetFascicoliEvento(int idEvento);
        int AddFascicoli(Fascicoli model);
        void UpdateFascicoli(Fascicoli model);
        void DeleteFascicoli(int id);
        Fascicoli GetFascicoliById(int id);
        bool ScambiaPriorita(int id1, int id2);
        List<Fascicoli> GetFascicoliAperti(int idOrganizzazione, int idEvento);
        List<Fascicoli> GetFascicoli();
        void ChiudiDocumentiFascicolo(int idFascicolo);
        void PubblicaFascicolo(int idFascicolo);
        void AnnullaPubblicazioneFascicolo(int idFascicolo);
        void EliminaFileFittizio(int idFascicoloSelezionato);

        //AssUtentiDocumenti
        IEnumerable<AssUtentiDocumenti> GetAssUtentiDocumenti(int idFascicolo);
        AssUtentiDocumenti GetAssUtentiDocumenti(int idDocumento, int idUtente);
        void AddAssUtentiDocumenti(AssUtentiDocumenti model);
        void DeleteAllAssUtentiDocumenti(int idFascicolo);

        //PreseInCarico
        int InsertPreseInCarico(PreseInCarico model);
        void Release(int idPresaInCarico);
        List<PreseInCarico> GetPreseInCaricoUtente(int idUtente, bool soloAperte);
    }

    public class GedService : IGedService
    {
        private readonly IDatabase _db;
        public GedService(IDatabase db)
        {
            _db = db;
        }

        #region Documenti
        public IEnumerable<Documenti> GetDocumenti(int idEvento, bool gestore, bool vedeRiservati, int idUtente)
        {
            var query = @"SELECT * FROM (SELECT Ged_Documenti.Id, Ged_Documenti.IdAdiuto, Ged_Documenti.IdFascicolo, Ged_Documenti.Note, 
                    Ged_Documenti.Nome, Ged_Documenti.Descrizione, Ged_Documenti.Priorita, 
                    Ged_Documenti.Versione, Ged_Documenti.DataInserimento, Ged_Documenti.DataFineValidita, Ged_Documenti.Riservato,
                    Ged_Documenti.Visibile, Ged_Documenti.IdImmagine, ISNULL(Ged_Fascicoli.Nome, 'ODG') AS FascicoloNome,
                    ISNULL(Ged_Fascicoli.Descrizione, '') AS FascicoloDescrizione, ISNULL(Ged_Fascicoli.Priorita, 0) AS FascicoloPriorita,
                    (REPLACE(STR(ISNULL(Ged_Fascicoli.Priorita, 0), 3), SPACE(1), '0') + '|' + 
                    ISNULL(Ged_Fascicoli.Nome, 'ODG') + '|' + 
                    CONVERT(VARCHAR, ISNULL(Ged_Fascicoli.Id, '0'))) + '|' +  
                    CONVERT(VARCHAR, (DENSE_RANK() OVER(Order By ISNULL(Ged_Fascicoli.Priorita, 0)) - 1) + 1) + '|' + 
                    CONVERT(VARCHAR, CASE WHEN Ged_Documenti.IdFascicolo IS NULL THEN -1 ELSE (SELECT COUNT(*) FROM Ged_Documenti AS gd1 WHERE 
                        (gd1.IdFascicolo = Ged_Documenti.IdFascicolo) AND (DataFineValidita IS NULL)) END) + '|' + 
                    CASE WHEN Ged_Documenti.IdFascicolo IS NULL THEN (CASE WHEN (SELECT Visibile FROM Ged_Documenti AS gd3 WHERE gd3.Id = Ged_Documenti.ID) = 1 
					     THEN 'Pubblicato' ELSE 'Non Pubblicato' END)
                    WHEN (SELECT count(*) FROM Ged_Documenti AS gd2 RIGHT OUTER JOIN Ged_AssEventiDocumenti AS ga2 ON ga2.IdDocumento = gd2.Id WHERE (ga2.IdEvento = @0) AND (gd2.IdFascicolo = Ged_Documenti.IdFascicolo)) = (SELECT count(*) FROM Ged_Documenti AS gd3 RIGHT JOIN Ged_AssEventiDocumenti AS ga3 ON ga3.IdDocumento = gd3.Id WHERE (ga3.IdEvento = @0) AND (gd3.IdFascicolo = Ged_Documenti.IdFascicolo AND gd3.Visibile = 1)) THEN 'Pubblicato'
                    WHEN (SELECT count(*) FROM Ged_Documenti AS gd4 RIGHT OUTER JOIN Ged_AssEventiDocumenti AS ga4 ON ga4.IdDocumento = gd4.Id WHERE (ga4.IdEvento = @0) AND (gd4.IdFascicolo = Ged_Documenti.IdFascicolo AND gd4.Visibile = 1)) = 0 THEN 'Non Pubblicato' ELSE 'Parzialmente Pubblicato' END + '|' +
					ISNULL(CONVERT(VARCHAR, (SELECT MAX(DataFineValidita) AS DataChiusura FROM Ged_Documenti as gd 
						RIGHT JOIN Ged_AssEventiDocumenti as ga ON gd.ID = ga.IdDocumento WHERE (ga.IdEvento = @0) 
						AND gd.IdFascicolo = Ged_Documenti.IdFascicolo), 103), '') AS FascicoloPrioritaNomeId,
						Ged_AssEventiDocumenti.IdEvento AS IdEvento,
						Ged_PreseInCarico.IdUtente AS IdUtenteInCarico,
						CASE WHEN Ged_PreseInCarico.IdUtente = @1 THEN 1 ELSE 0 END AS PuoRilasciare,
						CASE WHEN Ged_AssUtentiDocumenti.IdUtente IS NOT NULL AND Ged_Documenti.DataFineValidita IS NULL AND Ged_PreseInCarico.IdUtente IS NULL THEN 1 ELSE 0 END AS PuoPrendereInCarico
                FROM Ged_AssEventiDocumenti
				LEFT JOIN Ged_Documenti ON Ged_Documenti.ID = Ged_AssEventiDocumenti.IdDocumento
				LEFT JOIN Ged_Fascicoli ON Ged_Fascicoli.ID = Ged_Documenti.IdFascicolo
                LEFT JOIN Ged_PreseInCarico ON Ged_PreseInCarico.DataRilascio IS NULL AND Ged_PreseInCarico.IdDocumento = Ged_Documenti.Id
				LEFT JOIN Ged_AssUtentiDocumenti ON Ged_AssUtentiDocumenti.IdUtente = @1 AND Ged_AssUtentiDocumenti.IdDocumento = Ged_Documenti.Id
                WHERE (Ged_AssEventiDocumenti.IdEvento = @0)) Documenti";

            var ret = _db.Query<Documenti>(query, idEvento, idUtente).ToList();
            if (!gestore)
            {
                var temp = ret.GroupBy(t => t.FascicoloPrioritaNomeId).ToList();
                var gruppiVuoti = new List<string>();
                foreach (var group in temp)
                {
                    var nessunoPubblicato = group.All(item => !item.Visibile);
                    if (nessunoPubblicato)
                    {
                        gruppiVuoti.Add(group.Key);
                    }
                }

                ret = ret.Where(t => !gruppiVuoti.Contains(t.FascicoloPrioritaNomeId)).ToList();
                for (var i = 0; i < gruppiVuoti.Count; i++)
                {
                    var fascicoloString = gruppiVuoti[i];
                    var fascicolo = gruppiVuoti[i].Split('|');
                    if (string.IsNullOrEmpty(fascicolo[7]))
                    {
                        fascicoloString = $"{fascicolo[0]}|{fascicolo[1]}|{fascicolo[2]}|{fascicolo[3]}|{fascicolo[4]}|0|{fascicolo[6]}|{DateTime.Now.ToString("dd/MM/yyyy")}";
                    }
                    ret.Add(new Documenti
                    {
                        Nome = "Questo fascicolo non contiene nessun documento",
                        Visibile = true,
                        FascicoloPrioritaNomeId = fascicoloString,
                        Chiuso = true,
                        DataFineValidita = DateTime.Today,
                        DataInserimento = DateTime.Now,
                        Id = -i,
                        IdAdiuto = -1,
                        Priorita = 1,
                        Riservato = false,
                        Versione = 1
                    });
                }
            }
            return ret;
        }

        public int AddDocumenti(Documenti model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void UpdateDocumenti(Documenti model)
        {
            _db.Update(model);
        }

        public void DeleteDocumenti(int id)
        {
            _db.Delete<Documenti>(id);
        }

        public Documenti GetDocumentiById(int id)
        {
            return _db.FirstOrDefault<Documenti>(@"SELECT Ged_Documenti.*, Ged_AssEventiDocumenti.IdEvento, Ged_Eventi.Nome AS NomeEvento FROM Ged_Documenti 
                LEFT JOIN Ged_AssEventiDocumenti ON Ged_AssEventiDocumenti.IdDocumento = Ged_Documenti.Id
                LEFT JOIN Ged_Eventi ON Ged_AssEventiDocumenti.IdEvento = Ged_Eventi.Id
                WHERE Ged_Documenti.Id = @0", id);
        }

        public List<Documenti> GetDocumentiRicercaAvanzata(int idOrganizzazione, bool vedeRiservati)
        {
            var query = @"SELECT Ged_Documenti.Id, Ged_Documenti.IdAdiuto, Ged_Documenti.Nome, Ged_Documenti.Descrizione, Ged_Documenti.Versione, Ged_Documenti.DataFineValidita, 
                    Ged_Documenti.Riservato, Ged_Documenti.Visibile, Ged_Documenti.IdImmagine, Ged_Documenti.DataInserimento, Ged_Documenti.UtenteInserimento, Ged_Documenti.DataUltimaModifica, 
                    Ged_Documenti.UtenteUltimaModifica, Ged_Eventi.Nome AS NOMEEVENTO, Ged_Eventi.Descrizione AS DESCRIZIONEEVENTO, Ged_Eventi.Data AS DATAEVENTO, Ged_Documenti.Priorita, Ged_Documenti.NOTE, 
                    Ged_Documenti.IdFascicolo, CASE WHEN DataFineValidita IS NULL THEN 0 ELSE 1 END AS Chiuso
                FROM Ged_Eventi LEFT OUTER JOIN
                    Ged_AssEventiDocumenti ON Ged_Eventi.Id = Ged_AssEventiDocumenti.IdEvento LEFT OUTER JOIN
                    Ged_Documenti ON Ged_AssEventiDocumenti.IdDocumento = Ged_Documenti.Id
                WHERE IDORGANIZZAZIONE = @0 AND Ged_Documenti.IdAdiuto > 0";
            if (!vedeRiservati)
                query = query + " AND Ged_Documenti.RISERVATO = 0";
            return _db.Query<Documenti>(query, idOrganizzazione).ToList();
        }

        public List<Documenti> GetDocumentiInFascicolo(int idFascicolo, bool soloAperti)
        {
            var query = @"SELECT Ged_Documenti.*, CASE WHEN DataFineValidita IS NULL THEN 0 ELSE 1 END AS Chiuso
                FROM Ged_Documenti WHERE IdFascicolo = @0";
            if (soloAperti)
                query = query + " AND DataFineValidita IS NULL";
            return _db.Query<Documenti>(query, idFascicolo).ToList();
        }

        #endregion

        #region Eventi
        public IEnumerable<Eventi> GetEventi(int idOrganizzazione)
        {
            return _db.Query<Eventi>("WHERE IDORGANIZZAZIONE = @0 ORDER BY DATA DESC", idOrganizzazione).ToList();
        }

        public IEnumerable<Eventi> GetEventi(int idOrganizzazione, DateTime da, DateTime a, bool gestore, DateTime utenteValidoDal, DateTime utenteValidoAl)
        {
            var query = "WHERE IDORGANIZZAZIONE = @0 AND DATA > @1 AND DATA < @2 AND DATA >= @3 AND DATA <=@4 ";
            if (!gestore)
                query = query + "AND Visibile = 1 ";
            return _db.Query<Eventi>(query + "ORDER BY DATA DESC", idOrganizzazione, da, a, utenteValidoDal, utenteValidoAl).ToList();
        }

        public int AddEventi(Eventi model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void UpdateEventi(Eventi model)
        {
            _db.Update(model);
        }

        public void DeleteEventi(int id)
        {
            _db.Delete<Eventi>(id);
        }

        public Eventi GetEventiById(int id)
        {
            return _db.FirstOrDefault<Eventi>(@"SELECT * FROM GED_EVENTI WHERE ID = @0", id);
        }
        #endregion

        #region Organizzazioni
        public IEnumerable<Organizzazioni> GetOrganizzazioni()
        {
            return _db.Query<Organizzazioni>().ToList();
        }

        public IEnumerable<Organizzazioni> GetOrganizzazioni(int idUtente)
        {
            return _db.Fetch<Organizzazioni>(@"SELECT GED_ORGANIZZAZIONI.* FROM GED_ASSORGANIZZAZIONIUTENTI LEFT OUTER JOIN
                GED_ORGANIZZAZIONI ON GED_ASSORGANIZZAZIONIUTENTI.IDORGANIZZAZIONE = GED_ORGANIZZAZIONI.ID
                WHERE GED_ASSORGANIZZAZIONIUTENTI.IDUTENTE = @0 AND GED_ASSORGANIZZAZIONIUTENTI.DataInizioValidita <= @1 AND GED_ASSORGANIZZAZIONIUTENTI.DataFineValidita >= @1", idUtente, DateTime.Now).ToList();
        }

        public int AddOrganizzazioni(Organizzazioni model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void UpdateOrganizzazioni(Organizzazioni model)
        {
            _db.Update(model);
        }

        public void DeleteOrganizzazioni(int id)
        {
            _db.Delete<Organizzazioni>(id);
        }

        public Organizzazioni GetOrganizzazioniById(int id)
        {
            return _db.FirstOrDefault<Organizzazioni>(@"WHERE ID = @0", id);
        }
        #endregion

        #region Immagini
        public int InsertImmagine(Immagini immagine)
        {
            immagine.CreatedDate = DateTime.Now;
            _db.Insert(immagine);
            return immagine.Id;
        }

        public void DeleteImmagine(int id)
        {
            _db.Delete<Immagini>(id);
        }

        public void DeleteImmagine(string idImmagine)
        {
            if (!string.IsNullOrEmpty(idImmagine))
            {
                int delId;
                int.TryParse(idImmagine.Split('.')[0], out delId);
                if (delId != 1) //controllo aggiuntivo per evitare che venga cancellata l'immagine di default
                    DeleteImmagine(delId);
            }
        }

        public IEnumerable<Immagini> GetIcone()
        {
            return _db.Query<Immagini>().ToList();
        }

        public string GetIdIcona(string fileName)
        {
            var ret = _db.FirstOrDefault<Immagini>(" WHERE SUBSTRING(FILENAME, 0, CHARINDEX('.', FILENAME)) = @0", fileName.Split('.').Last());
            if (ret == null)
                return "0.png";
            return ret.Id + ret.Extension;
        }

        #endregion

        #region AssEventiDocumenti
        public int GetAssEventiDocumenti(int idDocumento, int idEvento)
        {
            return _db.First<AssEventiDocumenti>("WHERE IdEvento = @0 AND IdDocumento = @1", idEvento, idDocumento).Id;
        }

        public int AddAssEventiDocumenti(AssEventiDocumenti model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void DeleteAssEventiDocumenti(int id)
        {
            _db.Delete<AssEventiDocumenti>("WHERE ID = @0", id);
        }

        public void DeleteAssEventiDocumenti(int idDocumento, int idEvento)
        {
            _db.Delete<AssEventiDocumenti>("WHERE IdDocumento = @0 AND IdEvento = @1", idDocumento, idEvento);
        }

        public void BulkAddAssEventiDocumenti(List<AssEventiDocumenti> associazioni)
        {
            _db.InsertBulk(associazioni);
        }
        #endregion

        #region AssOrganizzazioniUtenti
        public IEnumerable<AssOrganizzazioniUtenti> GetAssOrganizzazioniUtenti(int idOrganizzazione)
        {
            return _db.Query<AssOrganizzazioniUtenti>(@"WHERE IDORGANIZZAZIONE = @0", idOrganizzazione).ToList();
        }

        public int AddAssOrganizzazioniUtenti(AssOrganizzazioniUtenti model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void DeleteAllAssOrganizzazioniUtenti(int idOrganizzazione)
        {
            _db.Delete<AssOrganizzazioniUtenti>("WHERE IDORGANIZZAZIONE = @0", idOrganizzazione);
        }

        public void BulkAddAssOrganizzazioniUtenti(List<AssOrganizzazioniUtenti> associazioni)
        {
            _db.InsertBulk(associazioni);
        }

        public bool IsUtenteAvanzato(int userId)
        {
            return _db.Query<int>("SELECT * FROM GED_ASSORGANIZZAZIONIUTENTI WHERE IDUTENTE = @0 AND RICERCAAVANZATA = 1", userId).Any();
        }
        #endregion

        #region Fascicoli
        public IEnumerable<Fascicoli> GetFascicoliEvento(int idEvento)
        {
            return _db.Query<Fascicoli>(@"SELECT  DISTINCT  Ged_Fascicoli.Id, Ged_Fascicoli.Nome,
                Ged_Fascicoli.Descrizione, Ged_Fascicoli.Priorita
                FROM Ged_Fascicoli LEFT OUTER JOIN
                Ged_Documenti ON Ged_Fascicoli.Id = Ged_Documenti.IdFascicolo LEFT OUTER JOIN
                Ged_AssEventiDocumenti ON Ged_Documenti.Id = Ged_AssEventiDocumenti.IdDocumento
                WHERE Ged_AssEventiDocumenti.IDEVENTO = @0", idEvento).ToList();
        }

        public int AddFascicoli(Fascicoli model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void UpdateFascicoli(Fascicoli model)
        {
            _db.Update(model);
        }

        public void DeleteFascicoli(int id)
        {
            _db.Delete<Fascicoli>(id);
        }

        public Fascicoli GetFascicoliById(int id)
        {
            return _db.FirstOrDefault<Fascicoli>(@"WHERE ID = @0", id);
        }

        public bool ScambiaPriorita(int id1, int id2)
        {
            try
            {
                var f1 = _db.First<Fascicoli>("WHERE ID = @0", id1);
                var f2 = _db.First<Fascicoli>("WHERE ID = @0", id2);

                var p = f1.Priorita;
                f1.Priorita = f2.Priorita;
                f2.Priorita = p;

                UpdateFascicoli(f1);
                UpdateFascicoli(f2);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Fascicoli> GetFascicoliAperti(int idOrganizzazione, int idEvento)
        {
            return _db.Query<Fascicoli>(@"SELECT distinct Ged_Eventi.Nome AS NOMEEVENTO, Ged_Eventi.Descrizione AS DESCRIZIONEEVENTO,
                     Ged_Eventi.Data AS DATAEVENTO, Ged_Fascicoli.Id, Ged_Fascicoli.Nome, Ged_Fascicoli.Descrizione
                FROM Ged_Documenti LEFT OUTER JOIN Ged_Fascicoli ON Ged_Documenti.IdFascicolo = Ged_Fascicoli.Id RIGHT OUTER JOIN
                     Ged_AssEventiDocumenti ON Ged_Documenti.Id = Ged_AssEventiDocumenti.IdDocumento RIGHT OUTER JOIN
                     Ged_Eventi ON Ged_AssEventiDocumenti.IdEvento = Ged_Eventi.Id
                WHERE (Ged_Documenti.DataFineValidita IS NULL) AND (NOT (Ged_Documenti.Id IS NULL)) AND (NOT (Ged_Fascicoli.Nome IS NULL))
                     AND (Ged_Eventi.IdOrganizzazione = @0) AND (Ged_Eventi.Id <> @1)", idOrganizzazione, idEvento).ToList();
        }

        public List<Fascicoli> GetFascicoli()
        {
            return _db.Fetch<Fascicoli>().ToList();
        }

        public void ChiudiDocumentiFascicolo(int idFascicolo)
        {
            _db.Update<Documenti>("SET DataFineValidita = @0 WHERE IdFascicolo = @1 AND DataFineValidita IS NULL", DateTime.Now, idFascicolo);
        }

        public void PubblicaFascicolo(int idFascicolo)
        {
            _db.Update<Documenti>("SET Visibile = 1 WHERE IdFascicolo = @0", idFascicolo);
        }

        public void AnnullaPubblicazioneFascicolo(int idFascicolo)
        {
            _db.Update<Documenti>("SET Visibile = 0 WHERE IdFascicolo = @0", idFascicolo);
        }

        public void EliminaFileFittizio(int idFascicoloSelezionato)
        {
            var documentiDaCancellare = _db.Query<Documenti>("WHERE IdFascicolo = @0 AND IdAdiuto = -1", idFascicoloSelezionato).ToList();
            foreach (var doc in documentiDaCancellare)
            {
                _db.Delete<AssEventiDocumenti>("WHERE IdDocumento = @0", doc.Id);
                _db.Delete<Documenti>("WHERE Id = @0", doc.Id);
            }
        }
        #endregion

        #region AssUtentiDocumenti
        public IEnumerable<AssUtentiDocumenti> GetAssUtentiDocumenti(int idFascicolo)
        {
            return _db.Query<AssUtentiDocumenti>(@"SELECT Ged_AssUtentiDocumenti.* FROM Ged_AssUtentiDocumenti LEFT JOIN Ged_Documenti ON Ged_AssUtentiDocumenti.IdDocumento = Ged_Documenti.Id WHERE Ged_Documenti.IdFascicolo= @0", idFascicolo).ToList();
        }

        public AssUtentiDocumenti GetAssUtentiDocumenti(int idDocumento, int idUtente)
        {
            return _db.FirstOrDefault<AssUtentiDocumenti>("WHERE IdDocumento = @0 AND IdUtente = @1", idDocumento, idUtente);
        }

        public void AddAssUtentiDocumenti(AssUtentiDocumenti model)
        {
            _db.Insert(model);
        }

        public void DeleteAllAssUtentiDocumenti(int idFascicolo)
        {
            _db.Delete<AssUtentiDocumenti>(@"DELETE Ged_AssUtentiDocumenti FROM Ged_AssUtentiDocumenti 
                LEFT JOIN Ged_Documenti ON Ged_Documenti.Id = Ged_AssUtentiDocumenti.IdDocumento WHERE Ged_Documenti.IdFascicolo = @0", idFascicolo);
        }
        #endregion

        #region PreseInCarico
        public int InsertPreseInCarico(PreseInCarico model)
        {
            _db.Insert(model);
            return model.Id;
        }

        public void Release(int idPresaInCarico)
        {
            _db.Execute("UPDATE Ged_PreseInCarico SET DataRilascio = GetDate() WHERE Id = @0", idPresaInCarico);
        }

        public List<PreseInCarico> GetPreseInCaricoUtente(int idUtente, bool soloAperte)
        {
            var query = "WHERE IdUtente = @0";
            if (soloAperte)
                query = query + " AND DataRilascio IS NULL";
            return _db.Fetch<PreseInCarico>(query, idUtente);
        }

        #endregion
    }
}
