﻿using System;
using NPoco;

namespace Service.Models
{
    [TableName("Ged_AssUtentiDocumenti")]
    public class AssUtentiDocumenti
    {
        public int IdUtente { get; set; }
        public int IdDocumento { get; set; }
        public DateTime DataAssegnazione { get; set; }
        public string EmailUtente { get; set; }
        public int IdAdminAssegnatario { get; set; }
        public string EmailAdminAssegnatario { get; set; }
    }
}