﻿using System;
using System.ComponentModel.DataAnnotations;
using NPoco;

namespace Service.Models
{
    [TableName("GED_IMMAGINI")]
    public class Immagini
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("FILENAME")]
        public string FileName { get; set; }

        [Column("EXTENSION")]
        public string Extension { get; set; }

        [Column("SIZE")]
        public long Size { get; set; }

        [Column("IMAGE")]
        public byte[] Image { get; set; }

        [Column("MODIFIEDDATE")]
        public DateTime? ModifiedDate { get; set; }

        [Column("CREATEDDATE")]
        public DateTime? CreatedDate { get; set; }
    }

    public class SaveImageReturn
    {
        public int Id { get; set; }
        public string Extension { get; set; }
    }
}
