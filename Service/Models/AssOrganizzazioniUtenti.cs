﻿using System;
using NPoco;

namespace Service.Models
{
    [TableName("GED_ASSORGANIZZAZIONIUTENTI")]

    public class AssOrganizzazioniUtenti
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("IDORGANIZZAZIONE")]
        public int IdOrganizzazione { get; set; }

        [Column("IDUTENTE")]
        public int IdUtente { get; set; }

        [Column("GESTORE")]
        public bool Gestore { get; set; }

        [Column("RICERCAAVANZATA")]
        public bool RicercaAvanzata { get; set; }

        [Column("VISUALIZZARISERVATI")]
        public bool VisualizzaRiservati { get; set; }

        public DateTime DataInizioValidita { get; set; }
        public DateTime DataFineValidita { get; set; }

    }
}