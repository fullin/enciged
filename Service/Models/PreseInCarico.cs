﻿using System;
using NPoco;

namespace Service.Models
{
    [TableName("Ged_PreseInCarico")]
    public class PreseInCarico
    {
        public int Id { get; set; }

        public int IdUtente { get; set; }

        public int IdDocumento { get; set; }

        public DateTime DataPresaInCarico { get; set; }

        public DateTime? DataRilascio { get; set; }
    }
}