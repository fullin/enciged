﻿using System;
using System.ComponentModel.DataAnnotations;
using NPoco;

namespace Service.Models
{
    [TableName("GED_EVENTI")]

    public class Eventi
    {
        [Column("ID")]
        public int Id { get; set; }

        [Required]
        [Column("IDORGANIZZAZIONE")]
        public int IdOrganizzazione { get; set; }

        [Required]
        [Column("NOME")]
        public string Nome { get; set; }

        [Column("DESCRIZIONE")]
        public string Descrizione { get; set; }

        [Required]
        [Column("DATA")]
        public DateTime Data { get; set; }

        [Required]
        [Column("VISIBILE")]
        public bool Visibile { get; set; }
    }
}