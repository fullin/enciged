﻿using System.ComponentModel.DataAnnotations;
using NPoco;

namespace Service.Models
{
    [TableName("GED_ORGANIZZAZIONI")]

    public class Organizzazioni
    {
        [Column("ID")]
        public int Id { get; set; }

        [Required]
        [Column("NOME")]
        public string Nome { get; set; }

        [Column("DESCRIZIONE")]
        public string Descrizione { get; set; }
    }
}