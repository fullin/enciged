﻿using System;
using NPoco;

namespace Service.Models
{
    [TableName("GED_FASCICOLI")]

    public class Fascicoli
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("NOME")]
        public string Nome { get; set; }

        [Column("DESCRIZIONE")]
        public string Descrizione { get; set; }

        [Column("PRIORITA")]
        public int Priorita { get; set; }

        //Usato in diverse sezioni
        [ResultColumn]
        [Column("IDEVENTO")]
        public int IdEvento { get; set; }

        //Usati nella sezione di recupero documenti dagli eventi passati
        [ResultColumn]
        [Column("NOMEEVENTO")]
        public string NomeEvento { get; set; }

        [ResultColumn]
        [Column("DESCRIZIONEEVENTO")]
        public string DescrizioneEvento { get; set; }

        [ResultColumn]
        [Column("DATAEVENTO")]
        public DateTime DataEvento { get; set; }
    }
}