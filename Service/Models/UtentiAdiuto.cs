﻿using NPoco;

namespace Service.Models
{
    [TableName("TUSE")]

    public class UtentiAdiuto
    {
        [Column("FIUS")]
        public int Id { get; set; }

        [Column("FPAS")]
        public string Password { get; set; }

        //[Column("FIDS")]
        //public int? FIDS { get; set; }

        [Column("FUNA")]
        public string Username { get; set; }

        [Column("FADM")]
        public int? AmministratoreAdiuto { get; set; }

        [Column("FEMA")]
        public string Email { get; set; }

        [Column("SOSPESO")]
        public bool Sospeso { get; set; }

        public bool UtenteAvanzato { get; set; }
        //[Column("FLAN")]
        //public string FLAN { get; set; }

        //[Column("FSTS")]
        //public string FSTS { get; set; }

        //[Column("SSO")]
        //public int SSO { get; set; }
    }
}
