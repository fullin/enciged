﻿using NPoco;

namespace Service.Models
{
    [TableName("GED_ASSEVENTIDOCUMENTI")]
    public class AssEventiDocumenti
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("IDEVENTO")]
        public int IdEvento { get; set; }

        [Column("IDDOCUMENTO")]
        public int IdDocumento { get; set; }
    }
}