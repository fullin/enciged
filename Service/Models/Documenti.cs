﻿using System;
using NPoco;

namespace Service.Models
{
    [TableName("GED_DOCUMENTI")]

    public class Documenti
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("IDADIUTO")]
        public int IdAdiuto { get; set; }

        [Column("IDFASCICOLO")]
        public int? IdFascicolo { get; set; }

        [Column("NOME")]
        public string Nome { get; set; }

        [Column("DESCRIZIONE")]
        public string Descrizione { get; set; }

        [Column("PRIORITA")]
        public int Priorita { get; set; }

        [Column("VERSIONE")]
        public int Versione { get; set; }

        [Column("DATAFINEVALIDITA")]
        public DateTime? DataFineValidita { get; set; }

        [Column("RISERVATO")]
        public bool Riservato { get; set; }

        [Column("VISIBILE")]
        public bool Visibile { get; set; }

        [Column("IDIMMAGINE")]
        public string IdImmagine { get; set; }

        [Column("DATAINSERIMENTO")]
        public DateTime DataInserimento { get; set; }

        [Column("UTENTEINSERIMENTO")]
        public string UtenteInserimento { get; set; }

        [Column("DATAULTIMAMODIFICA")]
        public DateTime? DataUltimaModifica { get; set; }

        [Column("UTENTEULTIMAMODIFICA")]
        public string UtenteUltimaModifica { get; set; }

        [Column("NOTE")]
        public string Note { get; set; }

        //Gestione della folder in visualizzazione Documenti
        [ResultColumn]
        [Column("FASCICOLOPRIORITANOMEID")]
        public string FascicoloPrioritaNomeId { get; set; }

        //Si trova in join su tutte le query
        [ResultColumn]
        [Column("IDEVENTO")]
        public int IdEvento { get; set; }

        //Usati nella sezione di salvataggio del documento da form
        [ResultColumn]
        public TipoFascicolo TipoFascicolo { get; set; }

        [ResultColumn]
        public int? IdFascicoloSelezionato { get; set; }

        [ResultColumn]
        public string NomeNuovoFascicolo { get; set; }

        [ResultColumn]
        public string DescrizioneNuovoFascicolo { get; set; }

        [ResultColumn]
        public string NomeNuovoFascicoloVuoto { get; set; }

        [ResultColumn]
        public string DescrizioneNuovoFascicoloVuoto { get; set; }

        //Usati nella ricerca avanzata
        [ResultColumn]
        public bool Chiuso { get; set; }

        [ResultColumn]
        public string NomeEvento { get; set; }

        [ResultColumn]
        public string DataEvento { get; set; }

        //Febbraio 2018: Aggiunti dati relativi alla presa in carico dei documenti
        [ResultColumn]
        public string IdUtenteInCarico { get; set; }

        [ResultColumn]
        public string NomeUtenteInCarico { get; set; }

        [ResultColumn]
        public bool PuoRilasciare { get; set; }

        [ResultColumn]
        public bool PuoPrendereInCarico { get; set; }
    }

    public enum TipoFascicolo
    {
        NoFascicolo = 0,
        SelezionaFascicolo = 1,
        NuovoFascicolo = 2,
        FascicoloVuoto = 3
    }
}