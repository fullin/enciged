﻿using NPoco;

namespace Service.Models
{
    [TableName("GED_NOTEUTENTE")]

    public class NoteUtente
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("IDUTENTE")]
        public int IdUtente { get; set; }

        [Column("IDDOCUMENTO")]
        public int IdDocumento { get; set; }

        [Column("NOTA")]
        public string Nota { get; set; }
    }
}