﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestioneElettronicaDocumenti.Extensions;
using GestioneElettronicaDocumenti.Viewmodels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Service;
using Service.Models;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class IconeController : Controller
    {
        private IGedService _gedService;

        public IconeController(IGedService gedService, IAdiutoService adiutoService)
        {
            _gedService = gedService;
        }

        public ActionResult Icone(FeedDto feed)
        {
            if (!string.IsNullOrEmpty(feed.Messaggio))
                ViewData.Add("Feed", feed);
            return View();
        }

        public ActionResult NewIcone()
        {
            ViewData.Add("titolo", "Aggiungi una nuova Icona");
            return View("IconeForm", new IconeDto());
        }

        public ActionResult SaveIcone(IEnumerable<HttpPostedFileBase> files, IconeDto model, string submit)
        {
            FeedDto feed;
            var fileList = files.ToList();
            if (fileList.Count == 1)
            {
                var ret = new Immagini();
                foreach (var img in fileList) //sempre una sola immagine
                {
                    if (img != null)
                    {
                        img.ToFileContent(ret);
                        _gedService.InsertImmagine(ret);
                    }
                }
                feed = new FeedDto
                {
                    Classe = FeedClassEnum.Good,
                    Messaggio = "Icona inserita con successo."
                };
            }
            else
            {
                ViewData.Add("titolo", "Aggiungi una nuova Icona");
                ViewData.Add("Feed", new FeedDto
                {
                    Classe = FeedClassEnum.Warning,
                    Messaggio = "Controlla di aver allegato correttamente il file."
                });
                return View("IconeForm", model);
            }
            return RedirectToAction("Icone", feed);
        }

        [HttpGet]
        public ActionResult _Ajax_Select_Icone([DataSourceRequest]DataSourceRequest request)
        {
            var model = _gedService.GetIcone().OrderBy(t => t.FileName).ToList();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult _Ajax_Delete_Icone([DataSourceRequest]DataSourceRequest request, IconeDto model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _gedService.DeleteImmagine(model.IdImmagine);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("ERR", ex.Message);
                }
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
    }
}