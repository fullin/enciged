﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using GestioneElettronicaDocumenti.Extensions;
using Service;
using Service.Models;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class ImmaginiController : Controller
    {

        private readonly IGedService _gedService;

        public ImmaginiController(IGedService gedService)
        {
            _gedService = gedService;
        }

        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> fileImage)
        {
            var ret = new Immagini();
            foreach (var img in fileImage) //sempre una sola immagine dato che l'upload multiplo è stato bloccato sulla view
            {
                img.ToFileContent(ret);
                _gedService.InsertImmagine(ret);
            }
            return Json(new SaveImageReturn { Id = ret.Id, Extension = ret.Extension });
            //return Json(string.Format("{0}{1}", ret.Id, ret.Extension));
        }

        public ActionResult DeleteImage(int id)
        {
            _gedService.DeleteImmagine(id);
            return Json(new { success = true });
        }
    }
}