﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GestioneElettronicaDocumenti.AdiJedWS;
using GestioneElettronicaDocumenti.Extensions;
using GestioneElettronicaDocumenti.Viewmodels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using Service;
using Service.Models;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class DocumentiController : Controller
    {
        private readonly IGedService _gedService;
        private readonly IAdiutoService _adiutoService;

        public DocumentiController(IGedService gedService, IAdiutoService adiutoService)
        {
            _gedService = gedService;
            _adiutoService = adiutoService;
        }

        #region Documenti
        public ActionResult Documenti(int idEvento, string messaggio, FeedClassEnum classe = FeedClassEnum.Info)
        {
            if (!string.IsNullOrEmpty(messaggio))
                ViewData.Add("Feed", new FeedDto
                {
                    Messaggio = messaggio,
                    Classe = classe
                });
            var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
            var profiloUtente = _gedService.GetAssOrganizzazioniUtenti(_gedService.GetEventiById(idEvento).IdOrganizzazione).First(t => t.IdUtente == utente.Id);
            var ret = _gedService.GetEventiById(idEvento);
            LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
            return View(new DocumentiDto
            {
                IdEvento = idEvento,
                NomeEvento = ret.Nome,
                DescrizioneEvento = ret.Descrizione,
                IsPubblicatoEvento = ret.Visibile,
                UtenteGestore = profiloUtente.Gestore,
                UtenteVedeRiservati = profiloUtente.VisualizzaRiservati
            });
        }

        public ActionResult PubblicaEvento(int idEvento, bool isPubblicato)
        {
            var evento = _gedService.GetEventiById(idEvento);
            evento.Visibile = !evento.Visibile;
            _gedService.UpdateEventi(evento);
            if (isPubblicato)
            {
                return RedirectToAction("Documenti", new { idEvento, messaggio = "Pubblicazione annullata", classe = FeedClassEnum.Good });
            }
            else
            {
                return RedirectToAction("Documenti", new { idEvento, messaggio = "Pubblicazione avvenuta con successo", classe = FeedClassEnum.Good });
            }
        }

        public ActionResult NewDocumenti(int idEvento, string nomeEvento, string descrizioneEvento)
        {
            LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
            ViewData.Add("titolo", nomeEvento);
            ViewData.Add("sottoTitolo", descrizioneEvento);
            var dt = DateTime.Now;
            return View("DocumentiForm", new Documenti
            {
                Visibile = false,
                Priorita = 1,
                Versione = 1,
                IdEvento = idEvento,
                TipoFascicolo = TipoFascicolo.NoFascicolo
            });
        }

        public ActionResult EditDocumenti(int id, int idEvento, string nomeEvento, string descrizioneEvento)
        {
            LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
            ViewData.Add("titolo", nomeEvento);
            ViewData.Add("sottoTitolo", descrizioneEvento);

            var ret = _gedService.GetDocumentiById(id);
            ret.IdFascicoloSelezionato = ret.IdFascicolo;
            ret.TipoFascicolo = ret.IdFascicolo == null ? TipoFascicolo.NoFascicolo : TipoFascicolo.SelezionaFascicolo;
            return View("DocumentiForm", ret);
        }

        public ActionResult SaveDocumenti(IEnumerable<HttpPostedFileBase> files, Documenti model, string submit, int idEvento)
        {
            var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
            FeedDto feed;
            if (ModelState.IsValid)
            {
                //Gestione fascicolo
                switch (model.TipoFascicolo)
                {
                    case TipoFascicolo.NoFascicolo:
                        model.IdFascicolo = null;
                        break;
                    case TipoFascicolo.SelezionaFascicolo:
                        model.IdFascicolo = model.IdFascicoloSelezionato;
                        _gedService.EliminaFileFittizio(model.IdFascicoloSelezionato.Value);
                        break;
                    case TipoFascicolo.NuovoFascicolo:
                        if (model.NomeNuovoFascicolo.Length > 0)
                        {
                            model.IdFascicolo = _gedService.AddFascicoli(new Fascicoli
                            {
                                Nome = model.NomeNuovoFascicolo,
                                Descrizione = model.DescrizioneNuovoFascicolo,
                                Priorita = (_gedService.GetFascicoliEvento(idEvento).Count() + 1) * 10,
                            });
                        }
                        else
                        {
                            LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                            ViewData.Add("Feed", new FeedDto
                            {
                                Classe = FeedClassEnum.Warning,
                                Messaggio = "Inserire il nome del nuovo Fascicolo."
                            });
                            return View("DocumentiForm", model);
                        }
                        break;
                    case TipoFascicolo.FascicoloVuoto:
                        if (model.NomeNuovoFascicoloVuoto.Length > 0)
                        {
                            model.IdFascicolo = _gedService.AddFascicoli(new Fascicoli
                            {
                                Nome = model.NomeNuovoFascicoloVuoto,
                                Descrizione = model.DescrizioneNuovoFascicoloVuoto,
                                Priorita = (_gedService.GetFascicoliEvento(idEvento).Count() + 1) * 10,
                            });
                            //Creo un file fittizio senza componente Adiuto
                            model.IdAdiuto = -1;
                            model.DataInserimento = DateTime.Now;
                            model.UtenteInserimento = utente.Username;
                            model.Nome = "Questo fascicolo non contiene nessun documento";
                            model.Versione = 1;
                            model.Visibile = true;
                            model.DataFineValidita = DateTime.Today;
                            model.Priorita = 0;
                            model.Riservato = false;
                            _gedService.AddDocumenti(model);
                            _gedService.AddAssEventiDocumenti(new AssEventiDocumenti
                            {
                                IdDocumento = model.Id,
                                IdEvento = idEvento
                            });
                            feed = new FeedDto
                            {
                                Classe = FeedClassEnum.Good,
                                Messaggio = "Fascicolo vuoto creato con successo."
                            };
                            return RedirectToAction("Documenti", new { idEvento, feed.Messaggio, feed.Classe });
                        }
                        LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                        ViewData.Add("Feed", new FeedDto
                        {
                            Classe = FeedClassEnum.Warning,
                            Messaggio = "Inserire il nome del nuovo Fascicolo vuoto."
                        });
                        return View("DocumentiForm", model);
                }

                //gestione allegato e salvataggio
                if (model.Id != 0) //modifica: il file può esserci o no (Controllo versione; ho già il model.Id)
                {
                    //Salvataggio del file in adiuto
                    var fileList = files.ToList();
                    if (fileList.Count == 1 && fileList[0] != null) //il file c'è
                    {
                        foreach (var file in fileList) //sempre una sola immagine
                        {
                            try
                            {
                                //Creo i parametri che mi servono per adiuto
                                var ints = new ArrayOfInt { int.Parse(ConfigurationManager.AppSettings["AdiutoIdUnivocoFile"]) };
                                var strings = new ArrayOfString { model.Id.ToString() };
                                var target = new MemoryStream();
                                file.InputStream.CopyTo(target);
                                var retAdiuto = AdiutoExtensions.CaricaFileAdiuto(target.ToArray(), file.FileName, int.Parse(ConfigurationManager.AppSettings["AdiutoIdFamiglia"]), ints, strings);
                                if (int.Parse(retAdiuto[0]) > 0) //Inserimento adiuto OK
                                {
                                    model.IdAdiuto = int.Parse(retAdiuto[0]);
                                    //Assegno l'icona
                                    model.IdImmagine = _gedService.GetIdIcona(file.FileName);
                                    model.Nome = file.FileName.Split('\\').Last();
                                    model.Versione = model.Versione + 1;
                                }
                                else //inserimento adiuto FALLITO
                                {
                                    LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                                    ViewData.Add("Feed", new FeedDto
                                    {
                                        Classe = FeedClassEnum.Warning,
                                        Messaggio = "Si è verificato un imprevisto durante l'inserimento della nuova versione del file in Adiuto: " + retAdiuto[1]
                                    });
                                    return View("DocumentiForm", model);
                                }
                            }
                            catch (Exception ex)
                            {
                                LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                                ViewData.Add("Feed", new FeedDto
                                {
                                    Classe = FeedClassEnum.Warning,
                                    Messaggio = "Si è verificato un imprevisto durante l'inserimento della nuova versione del file in Adiuto: " + ex.Message
                                });
                                return View("DocumentiForm", model);
                            }
                        }
                    }
                    model.DataUltimaModifica = DateTime.Now;
                    model.UtenteUltimaModifica = utente.Username;
                    _gedService.UpdateDocumenti(model);
                    feed = new FeedDto
                    {
                        Classe = FeedClassEnum.Good,
                        Messaggio = "Documento aggiornato con successo."
                    };
                }
                else //insert: obbligo di file allegato
                {
                    //Salvataggio del file in adiuto
                    var fileList = files.ToList();
                    if (fileList.Count == 1 && fileList[0] != null)
                    {
                        foreach (var file in fileList) //sempre una sola immagine
                        {
                            //Ottengo l'Id dal database
                            model.DataInserimento = DateTime.Now;
                            model.UtenteInserimento = utente.Username;
                            model.Nome = file.FileName.Split('\\').Last();
                            _gedService.AddDocumenti(model);
                            var idAssocazione = _gedService.AddAssEventiDocumenti(new AssEventiDocumenti
                            {
                                IdDocumento = model.Id,
                                IdEvento = idEvento
                            });

                            try
                            {
                                //Creo i parametri che mi servono per adiuto
                                var ints = new ArrayOfInt { int.Parse(ConfigurationManager.AppSettings["AdiutoIdUnivocoFile"]) };
                                var strings = new ArrayOfString { model.Id.ToString() };
                                var target = new MemoryStream();
                                file.InputStream.CopyTo(target);
                                var retAdiuto = AdiutoExtensions.CaricaFileAdiuto(target.ToArray(), file.FileName.Split('\\').Last(), int.Parse(ConfigurationManager.AppSettings["AdiutoIdFamiglia"]), ints, strings);
                                if (int.Parse(retAdiuto[0]) > 0) //Inserimento adiuto OK
                                {
                                    model.IdAdiuto = int.Parse(retAdiuto[0]);
                                    //Assegno l'icona
                                    model.IdImmagine = _gedService.GetIdIcona(file.FileName);
                                    _gedService.UpdateDocumenti(model);
                                }
                                else //inserimento adiuto FALLITO
                                {
                                    //rimuovo gli inserimenti dal database
                                    _gedService.DeleteAssEventiDocumenti(idAssocazione);
                                    _gedService.DeleteDocumenti(model.Id);
                                    LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                                    ViewData.Add("Feed", new FeedDto
                                    {
                                        Classe = FeedClassEnum.Warning,
                                        Messaggio = "Si è verificato un imprevisto durante l'inserimento del file in Adiuto: " + retAdiuto[1]
                                    });
                                    return View("DocumentiForm", model);
                                }
                            }
                            catch (Exception ex)
                            {
                                LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                                ViewData.Add("Feed", new FeedDto
                                {
                                    Classe = FeedClassEnum.Warning,
                                    Messaggio = "Si è verificato un imprevisto durante l'inserimento del file in Adiuto: " + ex.Message
                                });
                                return View("DocumentiForm", model);
                            }
                        }
                    }
                    else
                    {
                        LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                        ViewData.Add("Feed", new FeedDto
                        {
                            Classe = FeedClassEnum.Warning,
                            Messaggio = "E' obbligatorio allegare un file."
                        });
                        return View("DocumentiForm", model);
                    }

                    feed = new FeedDto
                    {
                        Classe = FeedClassEnum.Good,
                        Messaggio = "Documento creato con successo."
                    };
                }
            }
            else
            {
                LoadViewData(idEvento, _gedService.GetEventiById(idEvento).IdOrganizzazione);
                ViewData.Add("Feed", new FeedDto
                {
                    Classe = FeedClassEnum.Warning,
                    Messaggio = "Controlla di aver inserito dei valori validi."
                });
                return View("DocumentiForm", model);
            }
            return RedirectToAction("Documenti", new { idEvento, feed.Messaggio, feed.Classe });
        }

        [HttpGet]
        public ActionResult _Ajax_Select_Documenti([DataSourceRequest]DataSourceRequest request, int idEvento)
        {
            var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
            var profiloUtente = _gedService.GetAssOrganizzazioniUtenti(_gedService.GetEventiById(idEvento).IdOrganizzazione).First(t => t.IdUtente == utente.Id);
            var model = _gedService.GetDocumenti(idEvento, profiloUtente.Gestore, profiloUtente.VisualizzaRiservati, utente.Id);
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult _Ajax_Delete_Documenti([DataSourceRequest]DataSourceRequest request, Documenti model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _gedService.DeleteAssEventiDocumenti(_gedService.GetAssEventiDocumenti(model.Id, model.IdEvento));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("ERR", ex.Message);
                }
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Utility Documenti
        private void LoadViewData(int idEvento, int idOrganizzazione)
        {
            var fascicoliEvento = _gedService.GetFascicoliEvento(idEvento);
            ViewData.Add("slFascicoli", fascicoliEvento.Select(t => new SelectListFascicoli { Text = t.Nome, Value = t.Id.ToString() }).ToList());
            var utenti = _adiutoService.GetUserList().Where(u => _gedService.GetAssOrganizzazioniUtenti(idOrganizzazione).Select(t => t.IdUtente).ToList().Contains(u.Id))
                .Select(t => new SelectListItem { Text = t.Username, Value = t.Id.ToString() }).ToList();
            utenti.Insert(0, new SelectListItem { Text = "", Value = null });
            ViewData.Add("slUtenti", utenti);
        }

        [HttpGet]
        public string ChiudiFascicolo(int idFascicolo)
        {
            try
            {
                _gedService.ChiudiDocumentiFascicolo(idFascicolo);
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public string EliminaFascicolo(int idFascicolo, int idEvento)
        {
            try
            {
                foreach (var doc in _gedService.GetDocumentiInFascicolo(idFascicolo, false))
                {
                    _gedService.DeleteAssEventiDocumenti(doc.Id, idEvento);
                }
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet]
        public FileResult Scarica(string idAdiuto, string nome)
        {
            var bytes = AdiutoExtensions.ScaricaFileAdiuto(Convert.ToInt32(idAdiuto));
            return File(bytes, AdiutoExtensions.ReturnExtension(nome.Split('.').Last()), nome);
        }

        public ActionResult FastSave(IEnumerable<HttpPostedFileBase> files, int idFascicolo, int idEvento)
        {
            try
            {
                //Salvataggio del file in adiuto
                var fileList = files.ToList();
                if (fileList.Count == 1 && fileList[0] != null)
                {
                    var doc = new Documenti
                    {
                        Visibile = true,
                        Priorita = 1,
                        Versione = 1,
                        IdEvento = idEvento
                    };

                    if (idFascicolo != 0)
                    {
                        doc.IdFascicolo = idFascicolo;
                        _gedService.EliminaFileFittizio(idFascicolo);
                    }

                    foreach (var file in fileList) //sempre una sola immagine
                    {
                        //Ottengo l'Id dal database
                        doc.DataInserimento = DateTime.Now;
                        var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
                        doc.UtenteInserimento = utente.Username;
                        doc.Nome = file.FileName.Split('\\').Last();
                        doc.Id = _gedService.AddDocumenti(doc);
                        var idAssocazione = _gedService.AddAssEventiDocumenti(new AssEventiDocumenti
                        {
                            IdDocumento = doc.Id,
                            IdEvento = idEvento
                        });

                        //Creo i parametri che mi servono per adiuto
                        var ints = new ArrayOfInt { int.Parse(ConfigurationManager.AppSettings["AdiutoIdUnivocoFile"]) };
                        var strings = new ArrayOfString { doc.Id.ToString() };
                        var target = new MemoryStream();
                        file.InputStream.CopyTo(target);
                        var retAdiuto = AdiutoExtensions.CaricaFileAdiuto(target.ToArray(), file.FileName.Split('\\').Last(), int.Parse(ConfigurationManager.AppSettings["AdiutoIdFamiglia"]), ints, strings);
                        if (int.Parse(retAdiuto[0]) > 0) //Inserimento adiuto OK
                        {
                            doc.IdAdiuto = int.Parse(retAdiuto[0]);
                            //Assegno l'icona
                            doc.IdImmagine = _gedService.GetIdIcona(file.FileName);
                            _gedService.UpdateDocumenti(doc);
                        }
                        else //inserimento adiuto FALLITO
                        {
                            //rimuovo gli inserimenti dal database
                            _gedService.DeleteAssEventiDocumenti(idAssocazione);
                            _gedService.DeleteDocumenti(doc.Id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            return Content("");
        }
        #endregion

        #region Documenti Passati
        public ActionResult DocumentiPassati(int idEvento)
        {
            var evento = _gedService.GetEventiById(idEvento);
            var ret = new FascicoliPassatiDto
            {
                FascicoliList = _gedService.GetFascicoliAperti(evento.IdOrganizzazione, idEvento),
                IdEvento = idEvento,
                NomeEvento = evento.Nome,
                DescrizioneEvento = evento.Descrizione
            };
            var utenti = _adiutoService.GetUserList().Select(t => new SelectListItem { Text = t.Username, Value = t.Id.ToString() }).ToList();
            ViewData.Add("slUtenti", utenti);
            return View("DocumentiPassatiForm", ret);
        }

        public ActionResult SaveDocumentiPassati(FascicoliPassatiDto model, string submit)
        {
            var associazioni = new List<AssEventiDocumenti>();

            foreach (string control in Request.Form.Keys)
            {
                if (control.StartsWith("ckba_"))
                {
                    var idFascicolo = int.Parse(control.Split('_')[1]);
                    associazioni.AddRange(_gedService.GetDocumentiInFascicolo(idFascicolo, true).Select(doc => new AssEventiDocumenti
                    {
                        IdEvento = model.IdEvento,
                        IdDocumento = doc.Id
                    }));
                }
                if (control.StartsWith("ckbc_"))
                {
                    var idFascicolo = int.Parse(control.Split('_')[1]);
                    associazioni.AddRange(_gedService.GetDocumentiInFascicolo(idFascicolo, false).Select(doc => new AssEventiDocumenti
                    {
                        IdEvento = model.IdEvento,
                        IdDocumento = doc.Id
                    }));
                }
            }
            foreach (var ass in associazioni)
            {
                try
                {
                    _gedService.AddAssEventiDocumenti(ass);
                }
                catch
                {
                    //Lo gestisco così in caso di duplicazione (per documenti già associati precedentemente)
                }
            }
            return RedirectToAction("Documenti", new { model.IdEvento, messaggio = "Documenti inseriti con successo.", classe = FeedClassEnum.Good });
        }
        #endregion

        #region Ricerca Avanzata
        public ActionResult RicercaAvanzata(int idOrganizzazione, int idUtente)
        {
            var utenti = _adiutoService.GetUserList().Where(u => _gedService.GetAssOrganizzazioniUtenti(idOrganizzazione).Select(t => t.IdUtente).ToList().Contains(u.Id)).Select(t => new SelectListItem { Text = t.Username, Value = t.Id.ToString() }).ToList();
            utenti.Insert(0, new SelectListItem { Text = "", Value = null });
            ViewData.Add("slUtenti", utenti);
            ViewData.Add("slFascicoli", _gedService.GetFascicoli().Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString(), }).ToList());
            var profiloUtente = _gedService.GetAssOrganizzazioniUtenti(idOrganizzazione).First(t => t.IdUtente == idUtente);
            return View("RicercaAvanzata", new RicercaAvanzataDto
            {
                IdOrganizzazione = idOrganizzazione,
                IdUtente = idUtente,
                VedeRiservati = profiloUtente.VisualizzaRiservati,
                Gestore = profiloUtente.Gestore
            });
        }

        [HttpGet]
        public ActionResult _Ajax_Select_RicercaAvanzata([DataSourceRequest]DataSourceRequest request, int idOrganizzazione, bool vedeRiservati)
        {
            var model = _gedService.GetDocumentiRicercaAvanzata(idOrganizzazione, vedeRiservati).OrderByDescending(t => t.DataInserimento).ToList();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}