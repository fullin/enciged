﻿using Service;
using System.Web.Mvc;
using System.Web.Security;
using GestioneElettronicaDocumenti.Viewmodels;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginDto model, string returnUrl)
        {
            if (ModelState.IsValid && Membership.ValidateUser(model.UserName, model.Password))
            {
                return RedirectToLocal(returnUrl);
            }
            ViewData.Add("Feed", new FeedDto
            {
                Classe = FeedClassEnum.Danger,
                Messaggio = "Username o password errata."
            });
            ModelState.AddModelError("PWD", "Utente non abilitato o password incorretta; ritentare.");
            return View(model);
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            if (ModelState.IsValid)
            {
                FormsAuthentication.SignOut();
            }
            return RedirectToAction("Index", "Home");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect("~");
        }
    }
}