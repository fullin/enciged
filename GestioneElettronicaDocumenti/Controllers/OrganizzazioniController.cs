﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestioneElettronicaDocumenti.Viewmodels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Service;
using Service.Models;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class OrganizzazioniController : Controller
    {
        private IGedService _gedService;
        private IAdiutoService _adiutoService;

        public OrganizzazioniController(IGedService gedService, IAdiutoService adiutoService)
        {
            _gedService = gedService;
            _adiutoService = adiutoService;
        }

        public ActionResult Organizzazioni(FeedDto feed)
        {
            if (!string.IsNullOrEmpty(feed.Messaggio))
                ViewData.Add("Feed", feed);
            return View();
        }

        public ActionResult NewOrganizzazioni()
        {
            ViewData.Add("listUtentiAdiuto", _adiutoService.GetUserList().ToList());
            ViewData.Add("listAssOrganizzazioniUtenti", new List<AssOrganizzazioniUtenti>());
            ViewData.Add("titolo", "Aggiungi una nuova Organizzazione");
            return View("OrganizzazioniForm", new Organizzazioni());
        }

        public ActionResult EditOrganizzazioni(int id)
        {
            ViewData.Add("listUtentiAdiuto", _adiutoService.GetUserList().ToList());
            ViewData.Add("listAssOrganizzazioniUtenti", _gedService.GetAssOrganizzazioniUtenti(id).ToList());
            ViewData.Add("titolo", "Modifica Organizzazione");
            return View("OrganizzazioniForm", _gedService.GetOrganizzazioniById(id));
        }

        public ActionResult SaveOrganizzazioni(IEnumerable<HttpPostedFileBase> files, Organizzazioni model, string submit)
        {
            FeedDto feed;
            if (ModelState.IsValid)
            {
                if (_gedService.GetOrganizzazioniById(model.Id) == null)
                {
                    model.Id = _gedService.AddOrganizzazioni(model);
                    feed = new FeedDto
                    {
                        Classe = FeedClassEnum.Good,
                        Messaggio = "Organizzazione inserita con successo."
                    };
                }
                else
                {
                    _gedService.UpdateOrganizzazioni(model);
                    feed = new FeedDto
                    {
                        Classe = FeedClassEnum.Good,
                        Messaggio = "Organizzazione aggiornata con successo."
                    };
                }

                var associazioni = new List<AssOrganizzazioniUtenti>();

                foreach (var user in _adiutoService.GetUserList().ToList())
                {
                    var attiva = false;
                    var gestore = false;
                    var ricercaAvanzata = false;
                    var visualizzaRiservati = false;
                    var validoDa = DateTime.Now.Date;
                    var validoA = DateTime.MaxValue.Date;

                    foreach (string control in Request.Form.Keys)
                    {
                        if (control.Equals("ckbAttiva_" + user.Id))
                        {
                            attiva = true;
                        }
                        if (control.Equals("ckbGestore_" + user.Id))
                        {
                            gestore = true;
                        }
                        if (control.Equals("ckbRicercaAvanzata_" + user.Id))
                        {
                            ricercaAvanzata = true;
                        }
                        if (control.Equals("ckbVisualizzaRiservati_" + user.Id))
                        {
                            visualizzaRiservati = true;
                        }
                        if (control.Equals("dvda_" + user.Id))
                        {
                            if (Request.Form[control].HasValue())
                                validoDa = DateTime.Parse(Request.Form[control]);
                        }
                        if (control.Equals("dva_" + user.Id))
                        {
                            if (Request.Form[control].HasValue())
                                validoA = DateTime.Parse(Request.Form[control]);
                        }
                    }
                    if (attiva)
                        associazioni.Add(new AssOrganizzazioniUtenti
                        {
                            IdOrganizzazione = model.Id,
                            IdUtente = user.Id,
                            Gestore = gestore,
                            RicercaAvanzata = ricercaAvanzata,
                            VisualizzaRiservati = visualizzaRiservati,
                            DataInizioValidita = validoDa,
                            DataFineValidita = validoA
                        });
                }
                _gedService.DeleteAllAssOrganizzazioniUtenti(model.Id);
                foreach (var ass in associazioni)
                {
                    _gedService.AddAssOrganizzazioniUtenti(ass);
                }
            }
            else
            {
                ViewData.Add("listUtentiAdiuto", _adiutoService.GetUserList().ToList());
                if (_gedService.GetOrganizzazioniById(model.Id) == null)
                {
                    ViewData.Add("titolo", "Aggiungi una nuova Organizzazione");
                    ViewData.Add("listAssOrganizzazioniUtenti", new List<AssOrganizzazioniUtenti>());
                }
                else
                {
                    ViewData.Add("titolo", "Modifica Organizzazione");
                    ViewData.Add("listAssOrganizzazioniUtenti", _gedService.GetAssOrganizzazioniUtenti(model.Id).ToList());
                }
                ViewData.Add("Feed", new FeedDto
                {
                    Classe = FeedClassEnum.Warning,
                    Messaggio = "Controlla di aver inserito dei valori validi."
                });
                return View("OrganizzazioniForm", model);
            }
            return RedirectToAction("Organizzazioni", feed);
        }

        [HttpGet]
        public ActionResult _Ajax_Select_Organizzazioni([DataSourceRequest]DataSourceRequest request)
        {
            var model = _gedService.GetOrganizzazioni().OrderBy(t => t.Nome).ToList();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult _Ajax_Delete_Organizzazioni([DataSourceRequest]DataSourceRequest request, UtentiAdiuto model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _gedService.DeleteOrganizzazioni(model.Id);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("ERR", ex.Message);
                }
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
    }
}