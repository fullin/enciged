﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ILogger = Autofac.Extras.NLog.ILogger;

using GestioneElettronicaDocumenti.Extensions;
using GestioneElettronicaDocumenti.Viewmodels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using NLog;
using Service;
using Service.Models;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class FascicoliController : Controller
    {
        private readonly IGedService _gedService;
        private readonly IAdiutoService _adiutoService;
        private readonly ILogger _logService;

        public FascicoliController(IGedService gedService, IAdiutoService adiutoService, ILogger logService)
        {
            _gedService = gedService;
            _adiutoService = adiutoService;
            _logService = logService;
        }

        public ActionResult Fascicoli(int idEvento, string messaggio, FeedClassEnum classe = FeedClassEnum.Info)
        {
            if (!string.IsNullOrEmpty(messaggio))
                ViewData.Add("Feed", new FeedDto
                {
                    Messaggio = messaggio,
                    Classe = classe
                });
            var ret = _gedService.GetEventiById(idEvento);
            LoadViewData(idEvento, -1);
            return View(new FascicoliDto
            {
                IdEvento = idEvento,
                NomeEvento = ret.Nome,
                DescrizioneEvento = ret.Descrizione
            });
        }

        public ActionResult EditFascicoli(int id, int idEvento, string nomeEvento, string descrizioneEvento)
        {
            ViewData.Add("titolo", nomeEvento);
            ViewData.Add("sottoTitolo", descrizioneEvento);
            LoadViewData(idEvento, id);
            var ret = _gedService.GetFascicoliById(id);
            ret.IdEvento = idEvento;
            return View("FascicoliForm", ret);
        }

        public ActionResult Pubblica(int id, int idEvento, string nomeEvento, string descrizioneEvento)
        {
            try
            {
                _gedService.PubblicaFascicolo(id);
                return RedirectToAction("Fascicoli", new
                {
                    idEvento,
                    messaggio = "Pubblicazione avvenuta con successo.",
                    classe = FeedClassEnum.Good
                });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Fascicoli", new
                {
                    idEvento,
                    messaggio = "Pubblicazione non riuscita:" + ex.Message,
                    classe = FeedClassEnum.Danger
                });
            }
        }

        public async Task<ActionResult> SaveFascicoli(IEnumerable<HttpPostedFileBase> files, Fascicoli model, string submit, int idEvento)
        {
            FeedDto feed;
            if (ModelState.IsValid)
            {
                _gedService.UpdateFascicoli(model);
                //foreach (var documento in _gedService.GetDocumentiInFascicolo(model.Id, false))
                //{
                //    documento.IdUtenteAssegnato = model.IdUtenteAssegnato;
                //    _gedService.UpdateDocumenti(documento);
                //}
                feed = new FeedDto
                {
                    Classe = FeedClassEnum.Good,
                    Messaggio = "Fascicolo aggiornato con successo."
                };
            }
            else
            {
                LoadViewData(model.IdEvento, model.Id);
                ViewData.Add("Feed", new FeedDto
                {
                    Classe = FeedClassEnum.Warning,
                    Messaggio = "Controlla di aver inserito dei valori validi."
                });
                return View("FascicoliForm", model);
            }

            var oldAss = _gedService.GetAssUtentiDocumenti(model.Id).ToList();
            var newAss = new List<AssUtentiDocumenti>();
            var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
            foreach (var user in _adiutoService.GetUserList().Where(u => _gedService.GetAssOrganizzazioniUtenti(_gedService.GetEventiById(idEvento).IdOrganizzazione).Select(t => t.IdUtente).ToList().Contains(u.Id)).ToList())
            {
                foreach (var doc in _gedService.GetDocumentiInFascicolo(model.Id, true).ToList())
                {
                    foreach (string control in Request.Form.Keys)
                    {
                        if (control.Equals("ckbAss_" + user.Id + "_" + doc.Id))
                        {
                            newAss.Add(new AssUtentiDocumenti
                            {
                                IdDocumento = doc.Id,
                                IdUtente = user.Id,
                                DataAssegnazione = DateTime.Now,
                                EmailUtente = user.Email,
                                IdAdminAssegnatario = utente.Id,
                                EmailAdminAssegnatario = utente.Email
                            });
                        }
                    }
                }
            }
            _gedService.DeleteAllAssUtentiDocumenti(model.Id);
            foreach (var ass in newAss)
            {
                _gedService.AddAssUtentiDocumenti(ass);
            }

            //comunicazione via mail delle assegnazioni
            foreach (var ass in newAss)
            {
                if (!oldAss.Any(t => t.IdUtente == ass.IdUtente && t.IdDocumento == ass.IdDocumento))
                {
                    var message = new MailMessage
                    {
                        Subject = "EnciGed: Assegnazione documenti"
                    };
                    var body = "Ci sono nuovi documenti a voi assegnati nel fascicolo \"" + model.Nome + "\". " + 
                               "<br/><a href='" + Url.Action("Documenti", "Documenti", new {idEvento = model.IdEvento}, Request.Url.Scheme) +
                               "'>Clicca qui per visionare l'evento</a>";
                    try
                    {
                        message.To.Add(ass.EmailUtente);
                        message.Body = string.Concat(MailUtils.HtmlEmailHeader, body, MailUtils.HtmlEmailFooter);
                        message.IsBodyHtml = true;

                        var ret = await MailUtils.SendMail(message);
                    }
                    catch (Exception ex)
                    {
                        _logService.LogException(LogLevel.Error, "Conferma Handler ", ex);
                    }
                }
            }

            return RedirectToAction("Fascicoli", new { idEvento, feed.Messaggio, feed.Classe });
        }

        [HttpGet]
        public ActionResult _Ajax_Select_Fascicoli([DataSourceRequest]DataSourceRequest request, int idEvento)
        {
            var model = _gedService.GetFascicoliEvento(idEvento).OrderBy(t => t.Priorita).ThenBy(t => t.Nome).ToList();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        private void LoadViewData(int idEvento, int idFascicolo)
        {
            ViewData.Add("slUtenti", _adiutoService.GetUserList().Where(u => _gedService.GetAssOrganizzazioniUtenti(_gedService.GetEventiById(idEvento).IdOrganizzazione).Select(t => t.IdUtente).ToList().Contains(u.Id)).ToList());
            ViewData.Add("slDocumentiFascicolo", _gedService.GetDocumentiInFascicolo(idFascicolo, true).ToList());
            ViewData.Add("slAssociazioni", _gedService.GetAssUtentiDocumenti(idFascicolo).ToList());
        }

        public ActionResult AnnullaPubblicazione(int id, int idEvento, string nomeEvento, string descrizioneEvento)
        {
            try
            {
                _gedService.AnnullaPubblicazioneFascicolo(id);
                return RedirectToAction("Fascicoli", new
                {
                    idEvento,
                    messaggio = "Pubblicazione annullata con successo.",
                    classe = FeedClassEnum.Good
                });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Fascicoli", new
                {
                    idEvento,
                    messaggio = "Annullamento della pubblicazione non riuscito:" + ex.Message,
                    classe = FeedClassEnum.Danger
                });
            }
        }
    }
}
