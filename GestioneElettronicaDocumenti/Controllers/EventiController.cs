﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestioneElettronicaDocumenti.Viewmodels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Service;
using Service.Models;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class EventiController : Controller
    {
        private IGedService _gedService;

        public EventiController(IGedService gedService)
        {
            _gedService = gedService;
        }

        public ActionResult Eventi(int idOrganizzazione, string messaggio, FeedClassEnum classe = FeedClassEnum.Info)
        {
            //ViewData.Add("slOrganizzazioni", _gedService.GetOrganizzazioni().Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString() }).ToList());
            if (!string.IsNullOrEmpty(messaggio))
                ViewData.Add("Feed", new FeedDto
                {
                    Messaggio = messaggio,
                    Classe = classe
                });
            return View(new EventiDto { IdOrganizzazione = idOrganizzazione, NomeOrganizzazione = _gedService.GetOrganizzazioniById(idOrganizzazione).Nome });
        }

        public ActionResult NewEventi(int idOrganizzazione, string nomeOrganizzazione)
        {
            //ViewData.Add("slOrganizzazioni", _gedService.GetOrganizzazioni().Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString() }).ToList());
            ViewData.Add("titolo", nomeOrganizzazione);
            var dt = DateTime.Now;
            return View("EventiForm", new Eventi { Data = dt.AddSeconds(-dt.Second), IdOrganizzazione = idOrganizzazione, Visibile = false});
        }

        public ActionResult EditEventi(int id, string nomeOrganizzazione)
        {
            //ViewData.Add("slOrganizzazioni", _gedService.GetOrganizzazioni().Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString() }).ToList());
            ViewData.Add("titolo", nomeOrganizzazione);
            return View("EventiForm", _gedService.GetEventiById(id));
        }

        public ActionResult SaveEventi(IEnumerable<HttpPostedFileBase> files, Eventi model, string submit)
        {
            FeedDto feed;
            if (ModelState.IsValid)
            {
                if (model.Id != 0)
                {
                    _gedService.UpdateEventi(model);
                    feed = new FeedDto
                    {
                        Classe = FeedClassEnum.Good,
                        Messaggio = "Evento aggiornato con successo."
                    }; 
                }
                else
                {
                    _gedService.AddEventi(model);
                    feed = new FeedDto
                    {
                        Classe = FeedClassEnum.Good,
                        Messaggio = "Evento creato con successo."
                    };
                }
            }
            else
            {
                //ViewData.Add("slOrganizzazioni", _gedService.GetOrganizzazioni().Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString() }).ToList());
                ViewData.Add("Feed", new FeedDto
                {
                    Classe = FeedClassEnum.Warning,
                    Messaggio = "Controlla di aver inserito dei valori validi."
                });
                return View("EventiForm", model);
            }
            return RedirectToAction("Eventi", new { model.IdOrganizzazione , feed.Messaggio, feed.Classe});
        }

        [HttpGet]
        public ActionResult _Ajax_Select_Eventi([DataSourceRequest]DataSourceRequest request, int idOrganizzazione)
        {
            var model = _gedService.GetEventi(idOrganizzazione).ToList();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult _Ajax_Delete_Eventi([DataSourceRequest]DataSourceRequest request, Documenti model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _gedService.DeleteEventi(model.Id);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("ERR", ex.Message);
                }
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }
    }
}