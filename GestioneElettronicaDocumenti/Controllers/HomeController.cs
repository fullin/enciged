﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Service;
using Service.Models;
using System.Linq;
using System.Web.Security;
using GestioneElettronicaDocumenti.Viewmodels;
using Newtonsoft.Json;

namespace GestioneElettronicaDocumenti.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IGedService _gedService;

        public HomeController(IGedService gedService)
        {
            _gedService = gedService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var idUtente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData).Id;
            var organizzazioniAppartenenza = _gedService.GetOrganizzazioni(idUtente).ToList();
            var model = new IndexDto
            {
                IdUtente = idUtente,
                DataDa = DateTime.Today.AddYears(-3),
                DataA = DateTime.Today.AddMonths(1),
                MostraPrimi = true,
                GestoreOrganizzazioneSelezionata = false,
                RicercaAvanzataOrganizzazioneSelezionata = false,
                OrganizzazioniAppartenenza = GetOrgApp(organizzazioniAppartenenza)
            };

            switch (organizzazioniAppartenenza.Count)
            {
                case 0:
                    model.Feed = new FeedDto
                    {
                        Messaggio = "Utente non configurato o non ancora attivo; consultare il proprio amministratore.",
                        Classe = FeedClassEnum.Danger
                    };
                    model.EventiList = new List<Eventi>();
                    return View(model);
                case 1:
                    model.Feed = null;
                    model.IdOrganizzazioneSelezionata = organizzazioniAppartenenza[0].Id;
                    var utente = _gedService.GetAssOrganizzazioniUtenti(organizzazioniAppartenenza[0].Id).First(t => t.IdUtente == idUtente);
                    model.RicercaAvanzataOrganizzazioneSelezionata = utente.RicercaAvanzata;
                    model.GestoreOrganizzazioneSelezionata = utente.Gestore;
                    var validoDa = utente.DataInizioValidita;
                    var validoA = utente.DataFineValidita;
                    if (utente.Gestore)
                    {
                        validoDa = DateTime.Parse("2/2/1753");
                        validoA = DateTime.MaxValue;
                    }
                    model.EventiList = _gedService.GetEventi(organizzazioniAppartenenza[0].Id, model.DataDa, model.DataA, utente.Gestore, validoDa, validoA).ToList();
                    return View(model);
            }

            model.Feed = new FeedDto
            {
                Messaggio = "Selezionare un'organizzazione e premere il pulsante Cerca.",
                Classe = FeedClassEnum.Info
            };
            model.EventiList = new List<Eventi>();
            return View(model);
        }

        private List<SelectListItem> GetOrgApp(List<Organizzazioni> organizzazioniAppartenenza)
        {
            if (organizzazioniAppartenenza.Count == 0)
            {
                return new List<SelectListItem>();
            }

            if (organizzazioniAppartenenza.Count == 1)
            {
                return organizzazioniAppartenenza.Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString() }).ToList();
            }

            var ls = organizzazioniAppartenenza.Select(t => new SelectListItem { Text = t.Nome, Value = t.Id.ToString() }).ToList();
            ls.Insert(0, new SelectListItem { Text = "", Value = null });

            return ls;
        }

        [HttpPost]
        public ActionResult Index(IndexDto model)
        {
            var datiUtente = _gedService.GetAssOrganizzazioniUtenti(model.IdOrganizzazioneSelezionata).First(t => t.IdUtente == model.IdUtente);
            model.GestoreOrganizzazioneSelezionata = datiUtente.Gestore;
            model.RicercaAvanzataOrganizzazioneSelezionata = datiUtente.RicercaAvanzata;

            if (model.IdOrganizzazioneSelezionata > 0)
            {
                model.Feed = null;
                var validoDa = datiUtente.DataInizioValidita;
                var validoA = datiUtente.DataFineValidita;
                if (datiUtente.Gestore)
                {
                    validoDa = DateTime.Parse("2/2/1753");
                    validoA = DateTime.MaxValue.Date;
                }
                if (model.DataDa < validoDa)
                    model.DataDa = validoDa;
                if (model.DataA < DateTime.Parse("2/2/1753"))
                    model.DataA = validoA;
                if (model.DataA > validoA)
                    model.DataA = validoA;
                model.EventiList = _gedService.GetEventi(model.IdOrganizzazioneSelezionata, model.DataDa, model.DataA.AddHours(23).AddMinutes(59).AddSeconds(59), datiUtente.Gestore, validoDa, validoA).ToList();
                if (model.EventiList.Count == 0)
                    model.Feed = new FeedDto
                    {
                        Messaggio = "Nessun evento trovato. Modificare i parametri di ricerca.",
                        Classe = FeedClassEnum.Info
                    };
            }
            else
            {
                model.EventiList = new List<Eventi>();
                model.Feed = new FeedDto
                {
                    Messaggio = "Compilare tutti i campi e premere il pulsante Cerca.",
                    Classe = FeedClassEnum.Info
                };
            }
            model.OrganizzazioniAppartenenza = GetOrgApp(_gedService.GetOrganizzazioni(model.IdUtente).ToList());
            return View(model);
        }
    }
}