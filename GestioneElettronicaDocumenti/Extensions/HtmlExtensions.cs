﻿using System.Web;

namespace GestioneElettronicaDocumenti.Extensions
{
    public static class HtmlExtensions
    {
        public static string BoundCheckBox(string field)
        {
            return "# if (" + field + ") { #<input type='checkbox' checked='checked' disabled='disabled' id='" + field + "' name='" + field +
                   "' value='#= " + field + " #' /># } else { # <input type='checkbox' disabled='disabled' id='" + field + "' name='" +
                   field + "' value='#= " + field + " #' /># } #";
        }

        public static string BoundTextColumn(string field)
        {
            const int visibleMaxTextLength = 60;
            return "# if (" + field + " == null) { #<div id='" + field + "' name='" + field + "' ></div># } " +
                   "else if (" + field + ".length > " + visibleMaxTextLength + ") { #<div title=\"#= " + HttpUtility.HtmlEncode(field) + " #\" id='" + field + "' name='" + field + "' >#= " + field + ".substring(0, " + visibleMaxTextLength + ")#+...</div># } " +
                   "else { #<div id='" + field + "' name='" + field + "' >#= " + field + " #</div># } #";
        }
    }
}