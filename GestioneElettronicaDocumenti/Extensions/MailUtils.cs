﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;

namespace GestioneElettronicaDocumenti.Extensions
{
    public class MailUtils
    {
        public const string HtmlEmailHeader = "<html><head><title></title></head><body style='font-family:arial; font-size:12px;'>";

        public const string HtmlEmailFooter = "</body></html>";
        public static async Task<EmailSendResult> SendMail(MailMessage msg)
        {
            if (ConfigurationManager.AppSettings["UseAmazon"] == "true")
            {
                msg.From = new MailAddress(ConfigurationManager.AppSettings["MailerFromAmazonUser"]);

                var rawMessage = new RawMessage();
                using (var memoryStream = ConvertMailMessageToMemoryStream(msg))
                {
                    rawMessage.Data = memoryStream;
                }

                var request = new SendRawEmailRequest
                {
                    RawMessage = rawMessage
                };

                var awsAccessKey = ConfigurationManager.AppSettings["awsAccessKey"];
                var awsSecretKey = ConfigurationManager.AppSettings["awsSecretKey"];

                using (var client = new AmazonSimpleEmailServiceClient(awsAccessKey, awsSecretKey, RegionEndpoint.EUWest1))
                {
                    var ret = await client.SendRawEmailAsync(request);
                    //HostingEnvironment.QueueBackgroundWorkItem(async ct => { await client.SendEmailAsync(request, ct); });  
                    if (ret.HttpStatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(ret.MessageId))
                    {
                        return new EmailSendResult
                        {
                            Success = true,
                            Message = ret.MessageId
                        };
                    }
                    return new EmailSendResult
                    {
                        Success = false,
                        Message = "Send email failure"
                    };
                }
            }

            //else
            msg.From = new MailAddress(ConfigurationManager.AppSettings["MailerFromUser"]);
            try
            {
                var clientSmtp = new SmtpClient(ConfigurationManager.AppSettings["MailerHost"])
                {
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MailerUseSSL"]),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailerUser"], ConfigurationManager.AppSettings["MailerPassword"])
                };
                await clientSmtp.SendMailAsync(msg);
                return new EmailSendResult
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new EmailSendResult
                {
                    Success = false,
                    Message = "Send email failure: " + ex.Message
                };
            }
        }

        public static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message)
        {
            var assembly = typeof(SmtpClient).Assembly;
            var mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            var fileStream = new MemoryStream();
            var mailWriterContructor = mailWriterType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(Stream) }, null);
            var mailWriter = mailWriterContructor.Invoke(new object[] { fileStream });
            var sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
            sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true, false }, null);
            var closeMethod = mailWriter.GetType().GetMethod("Close", BindingFlags.Instance | BindingFlags.NonPublic);
            closeMethod.Invoke(mailWriter, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
            return fileStream;
        }
    }

    public class EmailSendResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}