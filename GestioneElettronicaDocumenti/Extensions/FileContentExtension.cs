﻿using System.Configuration;
using System.IO;
using System.Web;
using Service.Models;

namespace GestioneElettronicaDocumenti.Extensions
{
    public static class FileContentExtension
    {
        public static byte[] ToByteArray(this HttpPostedFileBase file)
        {
            byte[] data;
            using (var inputStream = file.InputStream)
            {
                var memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();
            }
            return data;
        }

        public static Immagini ToFileContent(this HttpPostedFileBase file, Immagini immagine)
        {
            immagine.Size = file.ContentLength;
            immagine.Image = file.ToByteArray();
            immagine.FileName = Path.GetFileName(file.FileName);
            immagine.Extension = Path.GetExtension(file.FileName);
            return immagine;
        }

        public static string ImageFriendlyUrl(this Immagini immagine)
        {
            return immagine == null ? null : Path.Combine(ConfigurationManager.AppSettings.Get("ImagePrefix"), string.Format("{0}{1}", immagine.Id, immagine.Extension));
        }
    }
}