﻿using System.Web;
using System.Web.Mvc;

namespace GestioneElettronicaDocumenti.Extensions
{
    public class SelectListFascicoli : SelectListItem
    {
        public int? IdUtenteAssegnato { get; set; }
    }
}