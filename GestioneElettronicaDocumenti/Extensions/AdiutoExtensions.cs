﻿using System;
using System.Configuration;
using GestioneElettronicaDocumenti.AdiJedWS;
using GestioneElettronicaDocumenti.SDKService;

namespace GestioneElettronicaDocumenti.Extensions
{
    public static class AdiutoExtensions
    {
        public static string[] CaricaFileAdiuto(byte[] fileData, string fileName, int idFamiglia, ArrayOfInt aInt, AdiJedWS.ArrayOfString aStr)
        {
            dynamic service = new AdiJedWSPortTypeClient();
            string sess;
            try
            {
                sess = service.remoteLogin(ConfigurationManager.AppSettings["AdiutoUser"], ConfigurationManager.AppSettings["AdiutoPassword"]);
            }
            catch (Exception ex)
            {
                return new string[] { "-10", "Impossibile creare una sessione: " + ex.Message };
            }

            string ret;
            try
            {
                ret = service.insertDocument(sess, fileData, fileName, idFamiglia, aInt, aStr);
                return new string[] { ret, ret };
            }
            catch (Exception ex)
            {
                //insert non riuscito
                service.remoteLogout(sess);
                return new string[] { "-200", "Chiamata di inserimento al servizio Adiuto non riuscita: " + ex.Message };
            }
        }

        public static byte[] ScaricaFileAdiuto(int idAdiuto)
        {
            var service = new SDKServicePortTypeClient();
            string sess;
            try
            {
                sess = service.remoteLogin(ConfigurationManager.AppSettings["AdiutoUser"], ConfigurationManager.AppSettings["AdiutoPassword"]);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return service.getLargeContent(sess, idAdiuto);
        }

        public static string ReturnExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                    return "text/HTML";
                case ".html":
                    return "text/HTML";
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".tiff":
                    return "image/tiff";
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                    return "image/jpeg";
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                    return "video/mpeg";
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                    return "application/xml";
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";

                case ".doc":
                    return "application/msword";
                case ".dot":
                    return "application/msword";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".dotx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
                case ".docm":
                    return "application/vnd.ms-word.document.macroEnabled.12";
                case ".dotm":
                    return "application/vnd.ms-word.template.macroEnabled.12";
                case ".xls":
                    return "application/vnd.ms-excel";
                case ".xlt":
                    return "application/vnd.ms-excel";
                case ".xla":
                    return "application/vnd.ms-excel";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xltx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
                case ".xlsm":
                    return "application/vnd.ms- excel.sheet.macroEnabled.12";
                case ".xltm":
                    return "application/vnd.ms-excel.template.macroEnabled.12";
                case ".xlam":
                    return "application/vnd.ms-excel.addin.macroEnabled.12";
                case ".xlsb":
                    return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
                case ".ppt":
                    return "application/vnd.ms-powerpoint";
                case ".pot":
                    return "application/vnd.ms-powerpoint";
                case ".pps":
                    return "application/vnd.ms-powerpoint";
                case ".ppa":
                    return "application/vnd.ms-powerpoint";
                case ".pptx":
                    return "application/vnd.openxmlformats-fficedocument.presentationml.presentation";
                case ".potx":
                    return "application/vnd.openxmlformats-officedocument.presentationml.template";
                case ".ppsx":
                    return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
                case ".ppam":
                    return "application/vnd.ms-powerpoint.addin.macroEnabled.12";
                case ".pptm":
                    return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
                case ".potm":
                    return "application/vnd.ms-powerpoint.template.macroEnabled.12";
                case ".ppsm":
                    return "application/vnd.ms-powerpoint.slideshow.macroEnabled.12";

                default:
                    return "application/octet-stream";
            }

        }
    }

}
