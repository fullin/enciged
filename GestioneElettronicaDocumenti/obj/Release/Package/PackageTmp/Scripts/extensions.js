﻿var jLabel;

function blinkFont(labelId) {
    jLabel = $('#' + labelId);
    setblinkFont();
}

function setblinkFont() {
    if (jLabel.length > 0) {
        //debugger;
        if (jLabel.css('color') == "rgb(255, 255, 255)") {
            jLabel.css('color', '#8B0000');
        } else {
            jLabel.css('color', '#ffffff');
        }
        setTimeout("setblinkFont()", 1000);
    }
}

function okImage() {
    $('#okImg').css('display', 'inline');
}

var matched, browser;

jQuery.uaMatch = function (ua) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
        /(webkit)[ \/]([\w.]+)/.exec(ua) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
        [];

    return {
        browser: match[1] || "",
        version: match[2] || "0"
    };
};

matched = jQuery.uaMatch(navigator.userAgent);
browser = {};

if (matched.browser) {
    browser[matched.browser] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if (browser.chrome) {
    browser.webkit = true;
} else if (browser.webkit) {
    browser.safari = true;
}

jQuery.browser = browser;