﻿using System.Globalization;
using System.Web.Optimization;
using BundleTransformer.Core.Bundles;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;

namespace GestioneElettronicaDocumenti
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            //bundles.UseCdn = true;
            var cssTransformer = new StyleTransformer();//CssTransformer();
            var jsTransformer = new ScriptTransformer();//JsTransformer();
            var nullOrderer = new NullOrderer();

            // jszip
            bundles.Add(new ScriptBundle("~/bundles/jszip").Include("~/Scripts/jszip.min.js"));

            // Vendor scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-2.1.1.min.js"));

            // Contextmenu
            bundles.Add(new ScriptBundle("~/bundles/contextmenu").Include("~/Scripts/bootstrap-contextmenu.js"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.validate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.min.js"));

            //RwdImageMaps
            bundles.Add(new ScriptBundle("~/bundles/rwdImageMaps").Include("~/Scripts/jquery.rwdImageMaps.js", "~/Scripts/jquery.rwdImageMaps.min.js"));

            // Inspinia script
            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include("~/Scripts/app/inspinia.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include("~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            // jQuery plugins
            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include("~/Scripts/plugins/metisMenu/jquery.metisMenu.js"));

            bundles.Add(new ScriptBundle("~/plugins/pace").Include("~/Scripts/plugins/pace/pace.min.js"));

            // CSS style
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.min.css", "~/Content/style.css", "~/Content/extensions.css"));

            // Font Awesome icons
            bundles.Add(new StyleBundle("~/font-awesome/css").Include("~/fonts/font-awesome-4.5.0/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            var cssKendoBundle = new CustomStyleBundle("~/bundles/css/kendo");
            cssKendoBundle.Include("~/Scripts/kendo/2015.1.408/styles/kendo.common-bootstrap.min.css", "~/Scripts/kendo/2015.1.408/styles/kendo.bootstrap.min.css");
            cssKendoBundle.Transforms.Add(cssTransformer);
            cssKendoBundle.Orderer = nullOrderer;
            bundles.Add(cssKendoBundle);

            var kendoBundle = new CustomScriptBundle("~/bundles/kendo");
            kendoBundle.Include("~/Scripts/kendo/2015.1.408/kendo.all.min.js", "~/Scripts/kendo/2015.1.408/kendo.aspnetmvc.min.js",
                string.Format("~/Scripts/kendo/2015.1.408/cultures/kendo.culture.{0}.min.js", CultureInfo.CurrentCulture),
                string.Format("~/Scripts/kendo/2015.1.408/messages/kendo.messages.{0}.min.js", CultureInfo.CurrentCulture));
            kendoBundle.Transforms.Add(jsTransformer);
            kendoBundle.Orderer = nullOrderer;
            bundles.Add(kendoBundle);

            var extensionsBundle = new CustomScriptBundle("~/bundles/extensions");
            extensionsBundle.Include("~/Scripts/extensions.js");
            extensionsBundle.Include("~/Scripts/moment-with-locales.min.js");
            extensionsBundle.Transforms.Add(jsTransformer);
            extensionsBundle.Orderer = nullOrderer;
            bundles.Add(extensionsBundle);
        }
    }
}
