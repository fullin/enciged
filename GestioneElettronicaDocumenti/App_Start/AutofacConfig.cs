﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using NPoco;
using Service;
using Autofac.Core;
using Autofac.Extras.NLog;
using Autofac.Integration.WebApi;
using Service.Infrastructure;

namespace GestioneElettronicaDocumenti
{
    public class AutofacConfig
    {
        public static void Startup()
        {
            var builder = new ContainerBuilder();
            var nomeDbGed = "Ged";
            var nomeDbAdiuto = "Adiuto";
            builder.RegisterType<GedDb>().Named<IDatabase>(nomeDbGed).InstancePerRequest();
            builder.RegisterType<AdiutoDb>().Named<IDatabase>(nomeDbAdiuto).InstancePerRequest();

            //logger
            builder.RegisterModule<NLogModule>();

            //Services
            builder.RegisterType<GedService>().As<IGedService>().WithParameter(ResolvedParameter.ForNamed<IDatabase>(nomeDbGed)).InstancePerRequest();
            builder.RegisterType<AdiutoService>().As<IAdiutoService>().WithParameter(ResolvedParameter.ForNamed<IDatabase>(nomeDbAdiuto)).InstancePerRequest();

            //HTTP context and other related stuff
            //register FakeHttpContext when HttpContext is not available
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}