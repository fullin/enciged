﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Security;
using GestioneElettronicaDocumenti.Extensions;
using Newtonsoft.Json;
using Service;
using Service.Models;
using LogLevel = NLog.LogLevel;
using ILogger = Autofac.Extras.NLog.ILogger;

namespace GestioneElettronicaDocumenti.Api
{
    public class GedApiController : ApiController
    {
        private readonly IGedService _gedService;
        private readonly ILogger _logService;

        public GedApiController(IGedService gedService, ILogger logService)
        {
            _gedService = gedService;
            _logService = logService;
        }

        [HttpGet]
        public bool ScambiaPriorita(int id1, int id2)
        {
            return _gedService.ScambiaPriorita(id1, id2);
        }

        [HttpGet]
        public PrendiInCaricoRet PrendiInCarico(string id)
        {
            var ret = new PrendiInCaricoRet();
            var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
            try
            {
                _gedService.InsertPreseInCarico(new PreseInCarico
                {
                    DataPresaInCarico = DateTime.Now,
                    IdDocumento = Convert.ToInt32(id),
                    IdUtente = utente.Id
                });
                ret.UserId = utente.Id.ToString();
            }
            catch (Exception ex)
            {
                ret.Ex = ex.Message;
            }
            return ret;
        }

        [HttpGet]
        public async Task<PrendiInCaricoRet> Rilascia(int id)
        {
            var ret = new PrendiInCaricoRet();
            var utente = JsonConvert.DeserializeObject<UtentiAdiuto>(((FormsIdentity)User.Identity).Ticket.UserData);
            try
            {
                var prese = _gedService.GetPreseInCaricoUtente(utente.Id, true);
                if (prese.Any(t => t.IdDocumento == id))
                {
                    var pic = prese.First(t => t.IdDocumento == id);
                    _gedService.Release(pic.Id);

                    var ass = _gedService.GetAssUtentiDocumenti(id, utente.Id);
                    if (ass != null)
                    {
                        var doc = _gedService.GetDocumentiById(id);
                        var message = new MailMessage
                        {
                            Subject = "EnciGed: Assegnazione documenti"
                        };
                        var body = "L'utente " + utente.Username + " (" + utente.Email + ") ha appena rilasciato il documento \"" + doc.Nome + "\" in relazione all'evento \"" + doc.NomeEvento + "\".";
                        try
                        {
                            message.To.Add(ass.EmailAdminAssegnatario);
                            message.Body = string.Concat(MailUtils.HtmlEmailHeader, body, MailUtils.HtmlEmailFooter);
                            message.IsBodyHtml = true;

                            var mailret = await MailUtils.SendMail(message);
                            if (!mailret.Success)
                            {
                                _logService.Log(LogLevel.Error, "Mailer: " + mailret.Message);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logService.LogException(LogLevel.Error, "Mailer: ", ex);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ret.Ex = ex.Message;
            }
            return ret;
        }
    }

    public class PrendiInCaricoRet
    {
        public string Ex { get; set; }
        public string UserId { get; set; }
    }
}