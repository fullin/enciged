﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestioneElettronicaDocumenti.Startup))]
namespace GestioneElettronicaDocumenti
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
