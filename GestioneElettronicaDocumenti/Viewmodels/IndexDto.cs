﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Service.Models;

namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class IndexDto
    {
        public int IdUtente { get; set; }
        [Required]
        public DateTime DataDa { get; set; }
        [Required]
        public DateTime DataA { get; set; }
        public FeedDto Feed { get; set; }
        [Required]
        public int IdOrganizzazioneSelezionata { get; set; }
        public bool GestoreOrganizzazioneSelezionata { get; set; }
        public bool RicercaAvanzataOrganizzazioneSelezionata { get; set; }
        public bool MostraPrimi { get; set; }
        public List<SelectListItem> OrganizzazioniAppartenenza { get; set; }
        public List<Eventi> EventiList { get; set; }
    }
}
