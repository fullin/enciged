﻿using System.Collections.Generic;
using Service.Models;

namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class FascicoliPassatiDto
    {
        public List<Fascicoli> FascicoliList { get; set; }

        public int IdEvento { get; set; }

        public string NomeEvento { get; set; }

        public string DescrizioneEvento { get; set; }
    }
}
