﻿namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class EventiDto
    {
        public int IdOrganizzazione { get; set; }
        public string NomeOrganizzazione { get; set; }
    }
}
