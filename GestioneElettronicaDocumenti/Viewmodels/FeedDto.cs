﻿using System;
using System.Web.Http.ValueProviders.Providers;
using GestioneElettronicaDocumenti.Helpers;

namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class FeedDto
    {
        public FeedClassEnum Classe { get; set; }
        public string Messaggio { get; set; }
    }

    public enum FeedClassEnum
    {
        [StringValue("feedback alert alert-danger")]
        Danger = 0,
        [StringValue("feedback alert alert-success")]
        Good = 1,
        [StringValue("feedback alert alert-info")]
        Info = 2,
        [StringValue("feedback alert alert-warning")]
        Warning = 3,
    }
}
