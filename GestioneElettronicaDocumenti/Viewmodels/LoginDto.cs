﻿using System.ComponentModel.DataAnnotations;

namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class LoginDto
    {
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
