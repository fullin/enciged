﻿namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class DocumentiDto
    {
        public int IdEvento { get; set; }
        public string NomeEvento { get; set; }
        public string DescrizioneEvento { get; set; }
        public bool IsPubblicatoEvento { get; set; }
        public bool UtenteGestore { get; set; }
        public bool UtenteVedeRiservati { get; set; }
    }
}
