﻿namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class FascicoliDto
    {
        public int IdEvento { get; set; }
        public string NomeEvento { get; set; }
        public string DescrizioneEvento { get; set; }
    }
}
