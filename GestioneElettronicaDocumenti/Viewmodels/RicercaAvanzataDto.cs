﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Service.Models;

namespace GestioneElettronicaDocumenti.Viewmodels
{
    public class RicercaAvanzataDto
    {
        public int IdOrganizzazione { get; set; }
        public int IdUtente { get; set; }
        public bool VedeRiservati { get; set; }
        public bool Gestore { get; set; }
    }
}
